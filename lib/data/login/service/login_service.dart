import 'package:cloud_firestore/cloud_firestore.dart';

abstract class LoginService{
  Future<DocumentSnapshot> login(String vin);
}

class LoginServiceImpl implements LoginService{
  final firebaseInstant = FirebaseFirestore.instance;
  @override
  Future<DocumentSnapshot> login(String vin) async {
    return firebaseInstant.collection("users").doc("vin").get();
  }

}