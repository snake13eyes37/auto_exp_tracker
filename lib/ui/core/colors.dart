import 'package:flutter/material.dart';

class AppColors {
  AppColors._();

  static const Color black = Color(0xFF333333);
  static const Color white = Color(0xFFFFFFFF);
  static const Color red = Color(0xFFC10000);
  static const Color grey = Color(0xFFD9D9D9);
  static const Color darkGrey = Color(0xFFC6C6C6);
  static const Color textFieldColor = Color(0xFF717171);
  static const Color transparent = Colors.transparent;
  static const Color accent = Color(0xFFFF7D7D);
  static const Color appBarBg = Color(0xFFFF9E9E);
  static const Color fuelColor = Color(0xFF1ACE0A);
  static const Color consumablesColor = Color(0xFF22F1F1);
  static const Color technicalPairColor = Color(0xFF584BEC);
  static const Color changeDetailColor = Color(0xFFF1229E);
  static const Color otherColor = Color(0xFFFDE720);
}
