import 'package:auto_exp_tracker/ui/core/colors.dart';
import 'package:flutter/material.dart';


class AppTextStyles {
  AppTextStyles._();

  static const _fontFamily = "Inter";

  static TextStyle extraBold10 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w900,
    fontSize: 10,
  );
  static TextStyle extraBold20 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w800,
    fontSize: 20,
  );
  static TextStyle extraBold16 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w800,
    fontSize: 16,
  );
  static TextStyle extraBold14 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w800,
    fontSize: 14,
  );
  static TextStyle bold20 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w700,
    fontSize: 20,
  );
  static TextStyle bold18 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w700,
    fontSize: 18,
  );
  static TextStyle bold16 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w700,
    fontSize: 16,
  );
  static TextStyle bold14 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w700,
    fontSize: 14,
  );
  static TextStyle bold12 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w700,
    fontSize: 12,
  );
  static TextStyle bold10 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w700,
    fontSize: 10,
  );
  static TextStyle semiBold24 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w600,
    fontSize: 24,
  );
  static TextStyle semiBold22 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w600,
    fontSize: 22,
  );
  static TextStyle semiBold18 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w600,
    fontSize: 18,
  );
  static TextStyle semiBold16 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w600,
    fontSize: 16,
  );
  static TextStyle semiBold15 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w600,
    fontSize: 15,
  );
  static TextStyle medium24 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w500,
    fontSize: 24,
  );
  static TextStyle medium22 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w500,
    fontSize: 22,
  );
  static TextStyle medium20 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w500,
    fontSize: 20,
  );
  static TextStyle medium18 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w500,
    fontSize: 18,
  );
  static TextStyle medium17 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w500,
    fontSize: 17,
  );
  static TextStyle medium16 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w500,
    fontSize: 16,
  );
  static TextStyle medium15 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w500,
    fontSize: 15,
  );
  static TextStyle medium14 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w500,
    fontSize: 14,
  );
  static TextStyle medium13 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w500,
    fontSize: 12,
  );
  static TextStyle medium12 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w500,
    fontSize: 12,
  );
  static TextStyle medium11 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w500,
    fontSize: 11,
  );
  static TextStyle regular32 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w400,
    fontSize: 32,
  );
  static TextStyle regular24 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w400,
    fontSize: 24,
  );
  static TextStyle regular22 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w400,
    fontSize: 22,
  );
  static TextStyle regular20 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w400,
    fontSize: 20,
  );
  static TextStyle regular18 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w400,
    fontSize: 18,
  );

  static TextStyle regular16 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w400,
    fontSize: 16,
  );
  static TextStyle regular15 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w400,
    fontSize: 15,
  );
  static TextStyle regular14 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w400,
    fontSize: 14,
  );
  static TextStyle regular12 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w400,
    fontSize: 12,
  );
  static TextStyle regular11 = const TextStyle(
    fontFamily: _fontFamily,
    color: AppColors.black,
    fontWeight: FontWeight.w400,
    fontSize: 11,
  );
}
