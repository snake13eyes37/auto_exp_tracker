class AppIcons {
  AppIcons._();

  static String _getSvgImage(String name) => 'assets/icons/$name.svg';

  static String logo = _getSvgImage("logo");
  static String arrowBack = _getSvgImage("back_arrow");
  static String avatarIcon = _getSvgImage("avatar_icon");
}
