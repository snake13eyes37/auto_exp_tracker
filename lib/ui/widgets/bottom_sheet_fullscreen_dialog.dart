import 'package:auto_exp_tracker/ui/core/colors.dart';
import 'package:flutter/material.dart';

Future<dynamic> showFullScreenBottomSheetDialog(
    BuildContext context,
    Widget body, {
      VoidCallback? onPressed,
      double paddingTop = 0,
    }) async {
  return await showModalBottomSheet(
    context: context,
    builder: (_) => BottomSheetFullScreenDialog(
      paddingTop: paddingTop,
      body: body,
      onPressed: onPressed,
    ),
    isScrollControlled: true,
  );
}

class BottomSheetFullScreenDialog extends StatelessWidget {
  final double paddingTop;
  final VoidCallback? onPressed;
  final Widget body;

  const BottomSheetFullScreenDialog({
    Key? key,
    this.onPressed,
    this.paddingTop = 0,
    required this.body,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height - paddingTop,
      decoration: const BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
      ),
      child: body,
    );
  }
}
