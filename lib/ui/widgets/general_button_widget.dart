import 'package:auto_exp_tracker/ui/core/colors.dart';
import 'package:auto_exp_tracker/ui/core/text_styles.dart';
import 'package:flutter/material.dart';

class GeneralButtonWidget extends StatelessWidget {
  final String text;
  final VoidCallback? onTap;
  final double? height;
  final double? minWidth;
  final EdgeInsets margins;
  final bool isOutlined;
  final Color? outlineColor;
  final double? borderRadius;
  final bool isButtonDisabled;
  final EdgeInsetsGeometry padding;
  final Widget? iconWidget;
  final Color textColor;
  final Color backgroundColor;

  final TextStyle? textStyle;

  GeneralButtonWidget({
    Key? key,
    this.text = '',
    this.onTap,
    this.height,
    this.margins = const EdgeInsets.all(0),
    this.minWidth,
    this.isOutlined = false,
    this.outlineColor,
    this.isButtonDisabled = false,
    this.borderRadius = 10,
    this.padding = const EdgeInsets.symmetric(horizontal: 24, vertical: 16),
    this.textStyle,
    this.iconWidget,
    this.textColor = AppColors.white,
    this.backgroundColor = AppColors.accent,
  }) : super(key: key);

  final TextStyle _defaultButtonTextStyle = AppTextStyles.regular24;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: margins,
      child: isOutlined
          ? _buildOutlinedButton(context)
          : _buildMaterialButton(context),
    );
  }

  Widget _buildMaterialButton(BuildContext context) {
    OutlinedBorder shape = const StadiumBorder();
    final radius = borderRadius;
    if (radius != null) {
      shape = RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(radius),
      );
    }
    const foregroundColor = AppColors.black;
    const bgColor = AppColors.accent;
    final buttonStyle = ButtonStyle(
      padding: MaterialStatePropertyAll<EdgeInsetsGeometry>(padding),
      textStyle: MaterialStateProperty.all(
        textStyle ?? _defaultButtonTextStyle,
      ),
      foregroundColor: const MaterialStatePropertyAll<Color>(foregroundColor),
      backgroundColor: const MaterialStatePropertyAll<Color>(bgColor),
      shape: MaterialStateProperty.all(shape),
      shadowColor: const MaterialStatePropertyAll<Color>(AppColors.transparent),
    );
    return SizedBox(
      height: height,
      width: minWidth,
      child: ElevatedButton(
        onPressed: isButtonDisabled ? null : onTap,
        style: buttonStyle,
        child: iconWidget ?? FittedBox(child: Text(text)),
      ),
    );
  }

  Widget _buildOutlinedButton(BuildContext context) {
    final foregroundColor =  textColor;
    final buttonStyle = ButtonStyle(
        padding: MaterialStatePropertyAll<EdgeInsetsGeometry>(padding),
        shape: MaterialStateProperty.all(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(borderRadius ?? 10),
          ),
        ),
        textStyle: MaterialStateProperty.all(
          textStyle ?? _defaultButtonTextStyle,
        ),
        foregroundColor: MaterialStatePropertyAll<Color>(foregroundColor));
    return SizedBox(
      height: height,
      width: minWidth,
      child: OutlinedButton(
        onPressed: onTap,
        style: buttonStyle,
        child: Text(text),
      ),
    );
  }
}
