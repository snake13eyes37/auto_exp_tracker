import 'package:auto_exp_tracker/core/extensions/value_extensions.dart';
import 'package:auto_exp_tracker/ui/core/colors.dart';
import 'package:auto_exp_tracker/ui/core/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';

class GeneralTextField extends StatefulWidget {
  final bool obscureText;
  final double? height;
  final AutovalidateMode autovalidateMode;
  final bool enabled;
  final bool autofocus;
  final Icon? suffixIcon;
  final VoidCallback? suffixIconTap;
  final String? suffixText;
  final TextInputType? keyboardType;
  final FormFieldValidator<String>? validator;
  final String? labelText;
  final String? floatingLabelText;
  final FloatingLabelBehavior? floatingLabelBehavior;
  final String? prefixText;
  final TextStyle? textStyle;
  final TextStyle? prefixStyle;
  final String? hintText;
  final Color? hintColor;
  final String formName;
  final FocusNode focusNode;
  final GlobalKey<FormBuilderState>? formKey;
  final Function(String?)? onChanged;
  final List<TextInputFormatter> inputFormatters;
  final int? maxLength;
  final TextEditingController? controller;
  final int maxLines;
  final int minLines;
  final EdgeInsets? contentPadding;
  final String? initialValue;
  final String? errorText;
  final TextCapitalization? textCapitalization;
  final Widget? prefixIcon;
  final BoxConstraints? prefixIconConstraints;
  final Color borderColor;
  final Color? labelColor;
  final bool enableInteractiveSelection;

  const GeneralTextField({
    Key? key,
    this.errorText,
    this.obscureText = false,
    this.autovalidateMode = AutovalidateMode.disabled,
    this.textStyle,
    this.suffixIcon,
    this.suffixIconTap,
    this.keyboardType,
    this.validator,
    this.labelText,
    this.prefixText,
    this.prefixStyle,
    this.floatingLabelText,
    this.floatingLabelBehavior,
    this.hintText,
    this.onChanged,
    this.enabled = true,
    this.autofocus = false,
    this.inputFormatters = const [],
    this.maxLength,
    this.controller,
    this.maxLines = 1,
    this.minLines = 1,
    this.contentPadding = const EdgeInsets.symmetric(vertical: 10),
    this.initialValue,
    this.height,
    this.suffixText,
    this.hintColor = AppColors.black,
    this.formKey,
    required this.formName,
    required this.focusNode,
    this.textCapitalization,
    this.prefixIcon,
    this.prefixIconConstraints,
    this.borderColor = AppColors.black,
    this.labelColor,
    this.enableInteractiveSelection = true,
  }) : super(key: key);

  @override
  _GeneralTextFieldState createState() => _GeneralTextFieldState();
}

class _GeneralTextFieldState extends State<GeneralTextField> {
  void listener() {
    setState(() {});
  }

  void initListener() {
    widget.focusNode.addListener(listener);
  }

  Color get _cursorColor => AppColors.black;

  TextStyle get _textStyle =>
      widget.textStyle ??
      AppTextStyles.regular16.copyWith(
        color: AppColors.black,
      );

  TextStyle get _labelTextStyle =>
      AppTextStyles.regular16.copyWith(color: AppColors.black);

  TextStyle get _floatingLabelTextStyle =>
      AppTextStyles.regular16.copyWith(color: AppColors.black);

  TextStyle get _hintTextStyle =>
      AppTextStyles.regular16.copyWith(color: widget.hintColor);

  TextStyle get _errorTextStyle =>
      AppTextStyles.regular16.copyWith(color: AppColors.red);

  TextStyle get _topLabelTextStyle => AppTextStyles.regular16.copyWith(
        color: _getTopLabelColor(),
      );

  Color _getTopLabelColor() {
    var isFieldHasError =
        widget.formKey?.currentState?.fields[widget.formName]?.hasError ??
            false;
    var color = AppColors.red;
    if (widget.focusNode.hasFocus) {
      if (isFieldHasError) {
        color = AppColors.red;
      } else {
        color = widget.labelColor ?? AppColors.black;
      }
    } else {
      if (isFieldHasError) {
        color = AppColors.red;
      } else {
        color = widget.labelColor ?? AppColors.black;
      }
    }
    return color;
  }

  @override
  void initState() {
    initListener();
    super.initState();
  }

  @override
  void dispose() {
    widget.focusNode.removeListener(listener);
    super.dispose();
  }

  void _onTextChange(String? text) {
    widget.onChanged?.call(text);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        _buildTopLabelWidget(),
        widget.height != null
            ? SizedBox(
                height: widget.height,
                child: _buildBody(),
              )
            : _buildBody()
      ],
    );
  }

  Column _buildBody() {
    return Column(
      children: [
        FormBuilderTextField(
          textCapitalization:
              widget.textCapitalization ?? TextCapitalization.none,
          keyboardAppearance: Brightness.light,
          autocorrect: false,
          autofocus: widget.autofocus,
          style: _textStyle,
          autovalidateMode: widget.autovalidateMode,
          initialValue: widget.initialValue,
          maxLines: widget.maxLines,
          minLines: widget.minLines,
          controller: widget.controller,
          inputFormatters: widget.inputFormatters,
          focusNode: widget.focusNode,
          name: widget.formName,
          obscureText: widget.obscureText,
          onChanged: _onTextChange,
          maxLength: widget.maxLength,
          decoration: _buildInputDecoration(),
          cursorColor: _cursorColor,
          validator: widget.validator,
          keyboardType: widget.keyboardType,
          enabled: widget.enabled,
          enableInteractiveSelection: widget.enableInteractiveSelection,
        ),
      ],
    );
  }

  InputDecoration _buildInputDecoration() {
    return InputDecoration(
      labelText: widget.floatingLabelText,
      prefixIconConstraints: widget.prefixIconConstraints ??
          const BoxConstraints(
            minWidth: 16,
            maxWidth: 16,
          ),
      prefixIcon: widget.prefixIcon,
      counterText: '',
      contentPadding: widget.contentPadding,
      hintText: valueOrEmpty(widget.hintText),
      hintStyle: _hintTextStyle,
      errorText: widget.errorText,
      errorStyle: _errorTextStyle,
      prefixText: widget.prefixText,
      prefixStyle: widget.prefixStyle,
      alignLabelWithHint: true,
      errorMaxLines: 3,
      labelStyle: _labelTextStyle,
      floatingLabelStyle: _floatingLabelTextStyle,
      floatingLabelBehavior: widget.floatingLabelBehavior,
      suffixText: widget.suffixText,
      suffixIcon: _buildSuffixIcon(),
      border: _getBorder(borderColor: AppColors.transparent),
      focusedBorder: _getBorder(borderColor: AppColors.transparent),
      enabledBorder: _getBorder(borderColor: AppColors.transparent),
      errorBorder: _getBorder(borderColor: AppColors.transparent),
    );
  }

  Widget? _buildSuffixIcon() {
    Widget? suffixIcon;

    if (widget.suffixIcon != null) {
      suffixIcon = GestureDetector(
        onTap: () {
          widget.suffixIconTap?.call();
        },
        child: widget.suffixIcon,
      );
    }

    return suffixIcon;
  }

  UnderlineInputBorder _getBorder({required Color borderColor}) {
    return UnderlineInputBorder(
      borderSide: BorderSide(color: borderColor),
    );
  }

  Widget _buildTopLabelWidget() {
    if (widget.labelText != null) {
      return Padding(
        padding: const EdgeInsets.only(bottom: 8),
        child: Text(
          valueOrEmpty(widget.labelText),
          style: _topLabelTextStyle,
        ),
      );
    }

    return const SizedBox.shrink();
  }
}
