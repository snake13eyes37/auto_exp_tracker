import 'package:auto_exp_tracker/ui/core/colors.dart';
import 'package:auto_exp_tracker/ui/core/icons.dart';
import 'package:auto_exp_tracker/ui/core/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class GeneralAppBar extends StatelessWidget {
  final bool isShowArrow;
  final Function? onProfileTap;

  const GeneralAppBar({
    Key? key,
    this.isShowArrow = true,
    this.onProfileTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.appBarBg,
      child: Padding(
        padding: const EdgeInsets.symmetric(
          horizontal: 0,
          vertical: 2,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _buildArrow(context),
            _buildLogo(),
            _buildAvatar(),
          ],
        ),
      ),
    );
  }

  Widget _buildArrow(BuildContext context) {
    return SizedBox(
      height: 45,
      width: 45,
      child: isShowArrow
          ? GestureDetector(
        onTap: (){
          Navigator.of(context).pop();
        },
            child: SvgPicture.asset(
                AppIcons.arrowBack,
                width: 45,
                height: 45,
              ),
          )
          : const SizedBox.shrink(),
    );
  }

  Widget _buildLogo() {
    return Text(
      "AutoExpensiveTracker",
      style: AppTextStyles.bold16,
    );
  }

  Widget _buildAvatar() {
    return Padding(
      padding: const EdgeInsets.only(right: 8),
      child: GestureDetector(
        onTap: () {
          onProfileTap?.call();
        },
        child: const Icon(
          Icons.account_circle,
          size: 49,
          color: AppColors.black,
        ),
      ),
    );
  }
}
