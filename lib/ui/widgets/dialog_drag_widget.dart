import 'package:flutter/material.dart';

class DialogDragWidget extends StatelessWidget {
  final double width;
  final double height;
  final EdgeInsets margins;
  final Color? color;

  const DialogDragWidget({
    Key? key,
    this.height = 5,
    this.width = 60,
    this.color,
    this.margins = const EdgeInsets.all(0),
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margins,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100),
        color: color ?? Theme.of(context).dividerColor,
      ),
      width: width,
      height: height,
    );
  }
}
