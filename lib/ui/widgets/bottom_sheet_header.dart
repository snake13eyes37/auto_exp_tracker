import 'package:auto_exp_tracker/ui/core/colors.dart';
import 'package:auto_exp_tracker/ui/core/text_styles.dart';
import 'package:flutter/material.dart';

import 'dialog_drag_widget.dart';

class BottomSheetHeader extends StatelessWidget {
  final IconData? iconData;
  final Color? iconColor;
  final String title;
  final TextStyle? titleStyle;
  final Function(BuildContext) onCloseIconPressed;

  const BottomSheetHeader({
    Key? key,
    this.iconData,
    this.iconColor,
    this.titleStyle,
    required this.onCloseIconPressed,
    required this.title,
  }) : super(key: key);

  TextStyle _getTextStyle(BuildContext context) {
    return titleStyle ?? AppTextStyles.medium17;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.appBarBg,
      child: Column(
        children: [
          const SizedBox(height: 8),
          const DialogDragWidget(),
          const SizedBox(height: 12),
          Stack(
            alignment: Alignment.centerLeft,
            children: [
              _buildIconButton(context),
              Center(
                child: Text(title, style: _getTextStyle(context)),
              ),
            ],
          ),
          Container(
            height: .5,
            color: AppColors.appBarBg,
          ),
        ],
      ),
    );
  }

  Widget _buildIconButton(BuildContext context) {
    return IconButton(
      iconSize: 26,
      onPressed: () => onCloseIconPressed(context),
      icon: Icon(
        iconData ?? Icons.close_rounded,
        color: iconColor ?? AppColors.black,
      ),
    );
  }
}
