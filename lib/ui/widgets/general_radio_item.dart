import 'package:auto_exp_tracker/ui/core/colors.dart';
import 'package:auto_exp_tracker/ui/core/text_styles.dart';
import 'package:flutter/material.dart';

class GeneralRadioItem extends StatelessWidget {
  final VoidCallback onTap;
  final String title;
  final bool isActive;

  const GeneralRadioItem({
    Key? key,
    required this.onTap,
    required this.title,
    required this.isActive,
  }) : super(key: key);



  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return InkWell(
      onTap: onTap,
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.symmetric(vertical: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                    child: Text(
                      title,
                      style: AppTextStyles.medium16,
                    ),
                  ),
                  _buildRadioIcon(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
  Widget _buildRadioIcon() {
    if (isActive) {
      return Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: AppColors.accent,
        ),
        width: 24,
        height: 24,
      );
    } else {
      return const SizedBox(height: 24, width: 24);
    }
  }
}
