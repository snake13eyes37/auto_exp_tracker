
import 'package:auto_exp_tracker/ui/core/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:cached_network_image/cached_network_image.dart';


class GeneralNetworkImage extends StatelessWidget {
  final String url;
  final BoxFit? fit;
  final double? size;
  const GeneralNetworkImage({
    Key? key,
    required this.url,
    this.fit,
    this.size,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (url.isEmpty) return _buildErrorWidget();
    return CachedNetworkImage(
      width:size ,
      height: size,
      imageUrl: url,
      fit: fit,
      placeholder: (_, __) {
        return Container(color: AppColors.white);
      },
      errorWidget: (_, __, ___) {
        return _buildErrorWidget();
      },
      imageBuilder: (context, imageProvider) {
        return Ink.image(
          image: imageProvider,
          fit: fit,
        );
      },
    );
  }

  Container _buildErrorWidget() {
    return Container(
      width: size,
      height: size,
      color: AppColors.grey,
      child: Center(
        child: Text("sorry somthing error"),
      ),
    );
  }
}

