import 'package:flutter/material.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:intl/intl.dart';

class Validators {
  const Validators._();

  static final RegExp _nameRegex = RegExp(r"^[ЁёА-яA-Za-z`'\-]*$");

  static FormFieldValidator<String?> required(BuildContext context) {
    return FormBuilderValidators.required(
      errorText: "Поле необхдимо для заполнения",
    );
  }

  static String? birthday(String? val) {
    final regex = _dateRegex();
    if (val == null || val.isEmpty) return null;
    DateTime? dateTime;
    try {
      dateTime = DateFormat('dd.MM.yyyy').parse(val);
    } catch (e) {
      dateTime = null;
    }
    if (dateTime == null) {
      return "Некорректная дата";
    }
    if (regex.hasMatch(val)) {
      return null;
    }
    return "Некорректная дата";
  }

  static RegExp _dateRegex() {
    const dayRegular = r'([12]\d|0[1-9]|3[01])';
    const monthRegular = r'(0[13-9]|1[12])';

    const dayFebruary = r'([12]\d|[0-2][1-9])';
    const monthFebruary = r'02';

    const year = r'(1[89]\d{2}|20[0-2]\d)';

    const regular = '$dayRegular\\.$monthRegular\\.$year';
    const february = '$dayFebruary\\.$monthFebruary\\.$year';

    return RegExp('($regular)|($february)');
  }
}
