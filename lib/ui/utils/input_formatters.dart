
import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class InputFormatters {
  InputFormatters._();

  static MaskTextInputFormatter phoneFormatter(
      mask, {
        String? initialText,
      }) {
    return MaskTextInputFormatter(
      mask: mask,
      filter: {"#": RegExp(r'[0-9]')},
      type: MaskAutoCompletionType.lazy,
      initialText: initialText,
    );
  }


  static TextInputFormatter get emailFormatter {
    return FilteringTextInputFormatter.allow(RegExp(r'[a-zA-Z]|[-_@.]|[0-9]'));
  }

  static DateFormat dateFormat = DateFormat('dd.MM.yyyy');

  static MaskTextInputFormatter dateFormatter({String? initialValue}) {
    return MaskTextInputFormatter(
      mask: dateFormat.pattern?.replaceAll(RegExp(r'[^.]'), '#'),
      initialText: initialValue,
    );
  }
}
