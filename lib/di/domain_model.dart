import 'package:auto_exp_tracker/presentation/login/mobx/login_store.dart';
import 'package:get_it/get_it.dart';

GetIt container = GetIt.instance;

void registerDi() {
  container.registerFactory<LoginStore>(() => LoginStore());
}
