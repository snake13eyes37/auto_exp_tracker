import 'dart:ui';

import 'package:auto_exp_tracker/ui/core/colors.dart';

enum ExpensiveType { fuel, consumables, service, changeDetails, other }

extension ExpensiveTypeExt on ExpensiveType {
  String get title {
    switch (this) {
      case ExpensiveType.fuel:
        return "Топливо";
      case ExpensiveType.consumables:
        return "Расходники";

      case ExpensiveType.service:
        return "Техническое обслуживание";

      case ExpensiveType.changeDetails:
        return "Замена деталей";

      case ExpensiveType.other:
        return "Прочее";
    }
  }

  Color get color {
    switch (this) {
      case ExpensiveType.fuel:
        return AppColors.fuelColor;
      case ExpensiveType.consumables:
        return AppColors.consumablesColor;
      case ExpensiveType.service:
        return AppColors.technicalPairColor;
      case ExpensiveType.changeDetails:
        return AppColors.changeDetailColor;
      case ExpensiveType.other:
        return AppColors.otherColor;
    }
  }

  String get toMap {
    switch (this) {
      case ExpensiveType.fuel:
        return "fuel";
      case ExpensiveType.consumables:
        return "consumables";
      case ExpensiveType.service:
        return "service";
      case ExpensiveType.changeDetails:
        return "changeDetails";
      case ExpensiveType.other:
        return "other";
    }
  }
}

ExpensiveType getExpTypeFromMap(String type) {
  switch (type) {
    case "fuel":
      return ExpensiveType.fuel;
    case "consumables":
      return ExpensiveType.consumables;
    case "service":
      return ExpensiveType.service;
    case "changeDetails":
      return ExpensiveType.changeDetails;
    default:
      return ExpensiveType.other;
  }
}
