import 'package:auto_exp_tracker/domain/profile/models/car_model.dart';

class ProfileModel {
  final String? name;
  final String? lastName;
  final int? years;
  final CarModel? car;

//<editor-fold desc="Data Methods">
  const ProfileModel({
    required this.name,
    required this.lastName,
    required this.years,
    required this.car,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is ProfileModel &&
          runtimeType == other.runtimeType &&
          name == other.name &&
          lastName == other.lastName &&
          years == other.years &&
          car == other.car);

  @override
  int get hashCode =>
      name.hashCode ^ lastName.hashCode ^ years.hashCode ^ car.hashCode;

  @override
  String toString() {
    return 'ProfileModel{' +
        ' name: $name,' +
        ' lastName: $lastName,' +
        ' years: $years,' +
        ' car: $car,' +
        '}';
  }

  ProfileModel copyWith({
    String? name,
    String? lastName,
    int? years,
    CarModel? car,
  }) {
    return ProfileModel(
      name: name ?? this.name,
      lastName: lastName ?? this.lastName,
      years: years ?? this.years,
      car: car ?? this.car,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'firstName': this.name,
      'lastName': this.lastName,
      'userYears': this.years,
      'vin': this.car?.vin,
      'nameAuto': this.car?.carType,
      'years': this.car?.years,
      'carRange': this.car?.range,
      'yearsBuy': this.car?.yearsBuy,
    };
  }

  factory ProfileModel.fromMap(Map<String, dynamic> map) {
    return ProfileModel(
      name: map['firstName'] as String?,
      lastName: map['lastName'] as String?,
      years: map['userYears'] as int?,
      car: CarModel(
        vin: map['vin'] as String?,
        carType: map['nameAuto'] as String?,
        years: map['years'] as int?,
        range: map['carRange'] as int?,
        url: map['carUrl'] as String?,
        yearsBuy: map['yearsBuy'] as int?,
      ),
    );
  }

//</editor-fold>
}
