import 'package:firebase_storage/firebase_storage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';


class CarModel{
  final String? vin;
  final String? carType;
  final int? years;
  final int? range;
  final String? url;
  final int? yearsBuy;

//<editor-fold desc="Data Methods">
  const CarModel({
    required this.vin,
    required this.carType,
    required this.years,
    required this.range,
    required this.url,
    required this.yearsBuy,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is CarModel &&
          runtimeType == other.runtimeType &&
          vin == other.vin &&
          carType == other.carType &&
          years == other.years &&
          range == other.range &&
          url == other.url &&
          yearsBuy == other.yearsBuy);

  @override
  int get hashCode =>
      vin.hashCode ^
      carType.hashCode ^
      years.hashCode ^
      range.hashCode ^
      url.hashCode ^
      yearsBuy.hashCode;

  @override
  String toString() {
    return 'CarModel{' +
        ' vin: $vin,' +
        ' carType: $carType,' +
        ' years: $years,' +
        ' range: $range,' +
        ' url: $url,' +
        ' yearsBuy: $yearsBuy,' +
        '}';
  }

  CarModel copyWith({
    String? vin,
    String? carType,
    int? years,
    int? range,
    String? url,
    int? yearsBuy,
  }) {
    return CarModel(
      vin: vin ?? this.vin,
      carType: carType ?? this.carType,
      years: years ?? this.years,
      range: range ?? this.range,
      url: url ?? this.url,
      yearsBuy: yearsBuy ?? this.yearsBuy,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'vin': this.vin,
      'carType': this.carType,
      'years': this.years,
      'range': this.range,
      'url': this.url,
      'yearsBuy': this.yearsBuy,
    };
  }

  factory CarModel.fromMap(Map<String, dynamic> map) {
    return CarModel(
      vin: map['vin'] as String,
      carType: map['carType'] as String,
      years: map['years'] as int,
      range: map['range'] as int,
      url: map['url'] as String,
      yearsBuy: map['yearsBuy'] as int,
    );
  }

  Future<String> get imageUrl async{

    final ref = FirebaseStorage.instance.ref().child('me.jpg');
    return await ref.getDownloadURL();
  }
//</editor-fold>
}