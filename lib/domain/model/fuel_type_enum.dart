enum FuelType { a95, a92, g95, g100, dt }

extension FuelTypeExt on FuelType {
  String get fuelName {
    switch (this) {
      case FuelType.a95:
        return "АИ-95";
      case FuelType.a92:
        return "АИ-92";
      case FuelType.g95:
        return "G-95";
      case FuelType.g100:
        return "100";
      case FuelType.dt:
        return "ДТ";
    }
  }
}

FuelType? getFuelTypeFromString(String? fuelType) {
  switch (fuelType) {
    case "a95":
      return FuelType.a95;
    case "a92":
      return FuelType.a92;
    case "g95":
      return FuelType.g95;
    case "g100":
      return FuelType.g100;
    case "dt":
      return FuelType.dt;
    default:
      return null;
  }
}
