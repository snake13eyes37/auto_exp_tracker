import 'package:auto_exp_tracker/domain/expensive/expensive_type.dart';
import 'package:auto_exp_tracker/domain/model/fuel_type_enum.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ExpensiveModel {
  final ExpensiveType type;
  final String description;
  final String expName;
  final FuelType? fuelType;
  final int kilometers;
  final int price;
  final int? liters;
  final DateTime date;
  final String id;

  Map<String, dynamic> get withoutNullValue {
    final map = toMap();
    map.removeWhere((key, value) => value == null);
    return map;
  }

//<editor-fold desc="Data Methods">
  const ExpensiveModel({
    required this.id,
    required this.type,
    required this.description,
    required this.expName,
    this.fuelType,
    required this.kilometers,
    required this.price,
    this.liters,
    required this.date,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is ExpensiveModel &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          type == other.type &&
          description == other.description &&
          expName == other.expName &&
          fuelType == other.fuelType &&
          kilometers == other.kilometers &&
          price == other.price &&
          liters == other.liters);

  @override
  int get hashCode =>
      id.hashCode ^
      type.hashCode ^
      description.hashCode ^
      expName.hashCode ^
      fuelType.hashCode ^
      kilometers.hashCode ^
      price.hashCode ^
      liters.hashCode;

  @override
  String toString() {
    return 'ExpensiveModel{' +
        ' id: $id' +
        ' type: $type,' +
        ' description: $description,' +
        ' expName: $expName,' +
        ' fuelType: $fuelType,' +
        ' kilometers: $kilometers,' +
        ' price: $price,' +
        ' liters: $liters,' +
        '}';
  }

  ExpensiveModel copyWith({
    ExpensiveType? type,
    String? description,
    String? expName,
    FuelType? fuelType,
    int? kilometers,
    int? price,
    int? liters,
    DateTime? date,
    String? id,
  }) {
    return ExpensiveModel(
      id: id ?? this.id,
      type: type ?? this.type,
      description: description ?? this.description,
      expName: expName ?? this.expName,
      fuelType: fuelType ?? this.fuelType,
      kilometers: kilometers ?? this.kilometers,
      price: price ?? this.price,
      liters: liters ?? this.liters,
      date: date ?? this.date,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': this.id,
      'expType': this.type.toMap,
      'description': this.description,
      'expName': this.expName,
      'fuelType': this.fuelType?.name,
      'kilometers': this.kilometers,
      'price': this.price,
      'liters': this.liters,
      'date': Timestamp.fromDate(date),
    };
  }

  factory ExpensiveModel.fromMap(Map<String, dynamic> map, String id) {
    return ExpensiveModel(
      id: id,
      type: getExpTypeFromMap(map['expType'] as String),
      description: map['description'] as String,
      expName: map['expName'] as String,
      fuelType: getFuelTypeFromString(map['fuelType'] as String?),
      kilometers: map['kilometers'] as int,
      price: map['price'] as int,
      liters: map['liters'] as int?,
      date: (map['date'] as Timestamp).toDate(),
    );
  }

//</editor-fold>
}
