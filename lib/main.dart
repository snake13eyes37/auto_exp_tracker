import 'package:auto_exp_tracker/di/domain_model.dart';
import 'package:auto_exp_tracker/presentation/app/app_page.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  registerDi();
  runApp(const AppWidget());
}