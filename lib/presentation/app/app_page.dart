import 'package:auto_exp_tracker/presentation/create_new_exp/create_new_exp_page.dart';
import 'package:auto_exp_tracker/presentation/create_new_exp/store/create_new_exp_store.dart';
import 'package:auto_exp_tracker/presentation/login/login_page.dart';
import 'package:auto_exp_tracker/presentation/statistic/statistic_page.dart';
import 'package:auto_exp_tracker/ui/utils/scroll_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

class AppWidget extends StatefulWidget {
  const AppWidget({Key? key}) : super(key: key);

  @override
  _AppWidgetState createState() => _AppWidgetState();
}

class _AppWidgetState extends State<AppWidget> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      builder: (context, child) {
        return MediaQuery(
          data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
          child: Stack(
            children: [
              ScrollConfiguration(
                behavior: ScrollConfig().copyWith(overscroll: false),
                child: child ?? const SizedBox.shrink(),
              ),
            ],
          ),
        );
      },
      home: const LoginPage(),
    );
  }
}
