import 'package:auto_exp_tracker/core/extensions/extensions.dart';
import 'package:auto_exp_tracker/domain/expensive/expensive_type.dart';
import 'package:auto_exp_tracker/domain/model/expensive_model.dart';
import 'package:auto_exp_tracker/domain/model/fuel_type_enum.dart';
import 'package:auto_exp_tracker/presentation/exp_detail/exp_detail_page.dart';
import 'package:auto_exp_tracker/presentation/exp_type_detail/mobx/exp_type_detail_store.dart';
import 'package:auto_exp_tracker/presentation/profile/profile_page.dart';
import 'package:auto_exp_tracker/ui/core/colors.dart';
import 'package:auto_exp_tracker/ui/core/text_styles.dart';
import 'package:auto_exp_tracker/ui/widgets/general_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';

ExpTypeDetailStore _store(context) =>
    Provider.of<ExpTypeDetailStore>(context, listen: false);

class ExpTypeDetailPage extends StatelessWidget {
  final ExpensiveType expensiveType;

  const ExpTypeDetailPage({
    Key? key,
    required this.expensiveType,
  }) : super(key: key);

  static MaterialPageRoute route(ExpensiveType type) => MaterialPageRoute(
        builder: (context) => ExpTypeDetailPage(expensiveType: type),
      );

  @override
  Widget build(BuildContext context) {
    return Provider<ExpTypeDetailStore>(
      create: (context) => ExpTypeDetailStore()..init(expensiveType),
      builder: (context, _) => const _StatisticScreen(),
    );
    ;
  }
}

class _StatisticScreen extends StatefulWidget {
  const _StatisticScreen({Key? key}) : super(key: key);

  @override
  State<_StatisticScreen> createState() => _StatisticScreenState();
}

class _StatisticScreenState extends State<_StatisticScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height -
              MediaQuery.of(context).padding.vertical,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              GeneralAppBar(
                isShowArrow: true,
                onProfileTap: () {
                  Navigator.of(context).push(ProfilePage.route());
                },
              ),
              _buildChart(context),
              const SizedBox(height: 24),
              Expanded(child: _buildExpensiveList(context)),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildChart(BuildContext context) {
    return Observer(builder: (context) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            _store(context).expensiveType.title,
            style: AppTextStyles.regular20,
          ),
          const SizedBox(
            height: 24,
          ),
          Text(
            "${_store(context).totalPrice} руб.",
            style: AppTextStyles.regular20,
          ),
          _buildRange(context),
        ],
      );
    });
  }

  Widget _buildRange(BuildContext context) {
    return Observer(builder: (context) {
      final range = _store(context).range;
      if (range == null) {
        return const SizedBox.shrink();
      }
      return Padding(
        padding: const EdgeInsets.only(top: 8),
        child: Text(
          "Средний расход топлива: ${range} л.\\100 км",
          style: AppTextStyles.regular15,
        ),
      );
    });
  }

  Widget _buildExpensiveList(BuildContext context) {
    return Observer(
      builder: (context) => Padding(
        padding: const EdgeInsets.all(24),
        child: Container(
          decoration: BoxDecoration(
            color: AppColors.grey,
            borderRadius: BorderRadius.circular(8),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              const SizedBox(height: 24),
              Text(
                "Затраты за период:",
                style: AppTextStyles.regular20,
              ),
              const SizedBox(height: 24),
              Expanded(
                child: ListView.separated(
                  itemBuilder: (context, index) => _buildExpensiveElement(
                    context,
                    _store(context).expensiveList.elementAt(index),
                  ),
                  separatorBuilder: (context, index) =>
                      const SizedBox(height: 16),
                  itemCount: _store(context).expensiveList.length,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildExpensiveElement(
    BuildContext context,
    ExpensiveModel expensive,
  ) {
    String expTitle = expensive.expName;
    if (expensive.type == ExpensiveType.fuel) {
      final fuelType = valueOrEmpty(expensive.fuelType?.fuelName);
      final liters = valueOrZero(expensive.liters);
      expTitle = "Заправка $fuelType ,$liters л.";
    }
    return GestureDetector(
      onTap: ()  async {
        await Navigator.of(context).push(ExpDetailPage.roure(expensive));
        _store(context).init(_store(context).expensiveType);
      },
      child: Container(
        decoration: BoxDecoration(
          color: AppColors.darkGrey,
          borderRadius: BorderRadius.circular(8),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(
            vertical: 14,
            horizontal: 8,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                expTitle,
                style: AppTextStyles.regular15,
              ),
              const SizedBox(height: 2),
              Text(
                "${expensive.kilometers} км. , ${expensive.price} руб.",
                style: AppTextStyles.regular15,
              ),
              const SizedBox(height: 2),
              Text(
                "Подробнее...",
                style: AppTextStyles.regular15,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
