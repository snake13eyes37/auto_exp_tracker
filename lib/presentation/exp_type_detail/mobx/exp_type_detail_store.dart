import 'package:auto_exp_tracker/core/enviroment/environment.dart';
import 'package:auto_exp_tracker/core/extensions/extensions.dart';
import 'package:auto_exp_tracker/domain/expensive/expensive_type.dart';
import 'package:auto_exp_tracker/domain/model/expensive_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dart_extensions/dart_extensions.dart';
import 'package:mobx/mobx.dart';

part 'exp_type_detail_store.g.dart';

class ExpTypeDetailStore = ExpTypeDetailStoreBase with _$ExpTypeDetailStore;

abstract class ExpTypeDetailStoreBase with Store {
  @observable
  ExpensiveType expensiveType = ExpensiveType.fuel;
  @observable
  List<ExpensiveModel> expensiveList = [];

  @computed
  int get totalPrice =>
      expensiveList.fold<int>(0, (pv, ExpensiveModel e) => pv + e.price);

  @computed
  double? get range {
    if (expensiveList.length < 2 || expensiveType != ExpensiveType.fuel) {
      return null;
    }
    final distance =
        expensiveList.first.kilometers - expensiveList.elementAt(1).kilometers;
    return valueOrZero(expensiveList.elementAt(1).liters) / distance * 100;
  }

  @action
  void init(ExpensiveType expensiveType) {
    this.expensiveType = expensiveType;
    initProfile();
  }

  void initProfile() async {
    final firebaseInstant = FirebaseFirestore.instance;
    final userID = Environment.vin;
    final result = await firebaseInstant
        .collection("user")
        .doc(userID)
        .get();
    final expensive = await result.reference
        .collection(expensiveType.name)
        // .where(
        //   "date",
        //   isGreaterThanOrEqualTo: Timestamp.fromDate(DateTime(2023, 5, 1)),
        //   isLessThanOrEqualTo: Timestamp.fromDate(DateTime(2023, 5, 6)),
        // )
        .get();
    expensiveList = expensive.docs
        .map((e) => ExpensiveModel.fromMap(e.data(), e.id))
        .toList()
      ..sort((a, b) => a.date.compareTo(b.date));
  }
}
