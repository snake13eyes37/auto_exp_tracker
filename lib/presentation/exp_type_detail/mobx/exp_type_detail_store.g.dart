// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'exp_type_detail_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$ExpTypeDetailStore on ExpTypeDetailStoreBase, Store {
  Computed<int>? _$totalPriceComputed;

  @override
  int get totalPrice =>
      (_$totalPriceComputed ??= Computed<int>(() => super.totalPrice,
              name: 'ExpTypeDetailStoreBase.totalPrice'))
          .value;
  Computed<double?>? _$rangeComputed;

  @override
  double? get range => (_$rangeComputed ??= Computed<double?>(() => super.range,
          name: 'ExpTypeDetailStoreBase.range'))
      .value;

  late final _$expensiveTypeAtom =
      Atom(name: 'ExpTypeDetailStoreBase.expensiveType', context: context);

  @override
  ExpensiveType get expensiveType {
    _$expensiveTypeAtom.reportRead();
    return super.expensiveType;
  }

  @override
  set expensiveType(ExpensiveType value) {
    _$expensiveTypeAtom.reportWrite(value, super.expensiveType, () {
      super.expensiveType = value;
    });
  }

  late final _$expensiveListAtom =
      Atom(name: 'ExpTypeDetailStoreBase.expensiveList', context: context);

  @override
  List<ExpensiveModel> get expensiveList {
    _$expensiveListAtom.reportRead();
    return super.expensiveList;
  }

  @override
  set expensiveList(List<ExpensiveModel> value) {
    _$expensiveListAtom.reportWrite(value, super.expensiveList, () {
      super.expensiveList = value;
    });
  }

  late final _$ExpTypeDetailStoreBaseActionController =
      ActionController(name: 'ExpTypeDetailStoreBase', context: context);

  @override
  void init(ExpensiveType expensiveType) {
    final _$actionInfo = _$ExpTypeDetailStoreBaseActionController.startAction(
        name: 'ExpTypeDetailStoreBase.init');
    try {
      return super.init(expensiveType);
    } finally {
      _$ExpTypeDetailStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
expensiveType: ${expensiveType},
expensiveList: ${expensiveList},
totalPrice: ${totalPrice},
range: ${range}
    ''';
  }
}
