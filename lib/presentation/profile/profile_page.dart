import 'package:auto_exp_tracker/core/extensions/extensions.dart';
import 'package:auto_exp_tracker/core/mocks/app_mocks.dart';
import 'package:auto_exp_tracker/presentation/edit_user/edit_user_page.dart';
import 'package:auto_exp_tracker/presentation/profile/mobx/profile_store.dart';
import 'package:auto_exp_tracker/ui/core/colors.dart';
import 'package:auto_exp_tracker/ui/core/text_styles.dart';
import 'package:auto_exp_tracker/ui/widgets/general_app_bar.dart';
import 'package:auto_exp_tracker/ui/widgets/general_button_widget.dart';
import 'package:auto_exp_tracker/ui/widgets/general_network_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';

ProfileStore _store(context) =>
    Provider.of<ProfileStore>(context, listen: false);

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  static MaterialPageRoute route() => MaterialPageRoute(
        builder: (context) => const ProfilePage(),
      );

  @override
  Widget build(BuildContext context) {
    return Provider<ProfileStore>(
      create: (context) => ProfileStore()..init(),
      builder: (context, _) => const _ProfileScreen(),
    );
  }
}

class _ProfileScreen extends StatefulWidget {
  const _ProfileScreen({Key? key}) : super(key: key);

  @override
  State<_ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<_ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const GeneralAppBar(isShowArrow: true),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Профиль",
                      style: AppTextStyles.regular20,
                    ),
                  ),
                  const SizedBox(height: 35),
                  Observer(
                    builder: (context) => Text(
                      "${valueOrEmpty(_store(context).profile?.name)},${valueOrZero(_store(context).profile?.years)} год ",
                      style: AppTextStyles.regular20,
                    ),
                  ),
                  const SizedBox(height: 30),
                  Text(
                    "Я владею 1 автомобилем",
                    style: AppTextStyles.regular20,
                  ),
                  const SizedBox(height: 25),
                  GeneralButtonWidget(
                    borderRadius: 10,
                    text: "Редактировать профиль",
                    minWidth: double.infinity,
                    onTap: () async {
                      await Navigator.of(context).push(
                        EditUserPage.route(_store(context).profile),
                      );
                      _store(context).init();
                    },
                  ),
                  const SizedBox(height: 70),
                  _buildAutoInfo(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildAutoInfo() {
    return Column(
      children: [
        Text(
          "Мой автомобиль:",
          style: AppTextStyles.regular20,
        ),
        const SizedBox(height: 24),
        Container(
          decoration: BoxDecoration(
            color: AppColors.grey,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 22),
            child: Observer(
              builder: (context) {
                final profile = _store(context).profile;
                if (profile == null) return const SizedBox.shrink();
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(71),
                          child: GeneralNetworkImage(
                            url: _store(context).image,
                            size: 172,
                            fit: BoxFit.fill,
                          ),
                        ),
                        Text(
                          valueOrEmpty(profile.car?.carType?.toUpperCase()),
                          style: AppTextStyles.regular24,
                        ),
                      ],
                    ),
                    const SizedBox(height: 23),
                    Text(
                      "Пробег: ${valueOrZero(profile.car?.range)} км",
                      style: AppTextStyles.regular20,
                    ),
                    const SizedBox(height: 23),
                    Text(
                      "Год: ${valueOrZero(profile.car?.years)}",
                      style: AppTextStyles.regular20,
                    ),
                    const SizedBox(height: 23),
                    Text(
                      "VIN: ${valueOrEmpty(profile.car?.vin)}",
                      style: AppTextStyles.regular20,
                    ),
                  ],
                );
              },
            ),
          ),
        ),
      ],
    );
  }
}
