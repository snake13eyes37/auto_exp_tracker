import 'package:auto_exp_tracker/core/enviroment/environment.dart';
import 'package:auto_exp_tracker/domain/profile/models/profile_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mobx/mobx.dart';

part 'profile_store.g.dart';

class ProfileStore = ProfileStoreBase with _$ProfileStore;

abstract class ProfileStoreBase with Store {
  @observable
  ProfileModel? profile;

  @observable
  String image = "";

  @action
  void init() {
    initProfile();
  }

  void initProfile() async {
    final firebaseInstant = FirebaseFirestore.instance;
    final userID = Environment.vin;
    final result = await firebaseInstant.collection("user").doc(userID).get();
    final data = result.data();
    if (data != null) {
      profile = ProfileModel.fromMap(data);
      profile?.car?.imageUrl.then((value) {
        image = value;
        print("fuuck ref ${value}");
      });
    }
  }
}
