import 'package:auto_exp_tracker/core/extensions/extensions.dart';
import 'package:auto_exp_tracker/domain/profile/models/profile_model.dart';
import 'package:auto_exp_tracker/presentation/edit_user/mobx/edit_user_store.dart';
import 'package:auto_exp_tracker/ui/core/colors.dart';
import 'package:auto_exp_tracker/ui/core/text_styles.dart';
import 'package:auto_exp_tracker/ui/widgets/general_app_bar.dart';
import 'package:auto_exp_tracker/ui/widgets/general_button_widget.dart';
import 'package:auto_exp_tracker/ui/widgets/general_text_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';

EditUserStore _store(context) =>
    Provider.of<EditUserStore>(context, listen: false);

class EditUserPage extends StatelessWidget {
  final ProfileModel? profileModel;

  const EditUserPage({
    Key? key,
    required this.profileModel,
  }) : super(key: key);

  static MaterialPageRoute route(ProfileModel? profileModel) =>
      MaterialPageRoute(
        builder: (context) => EditUserPage(profileModel: profileModel),
      );

  @override
  Widget build(BuildContext context) {
    return Provider<EditUserStore>(
      create: (context) => EditUserStore()..initProfile(),
      builder: (context, _) => _EditProfileScreen(profileModel: profileModel),
    );
  }
}

class _EditProfileScreen extends StatefulWidget {
  final ProfileModel? profileModel;

  const _EditProfileScreen({
    Key? key,
    required this.profileModel,
  }) : super(key: key);

  @override
  State<_EditProfileScreen> createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<_EditProfileScreen> {
  final String _nameFormName = "nameFormName";
  final _nameFocusNode = FocusNode();
  final _formKey = GlobalKey<FormBuilderState>();
  final String _lastNameFormName = "lastNameFormName";
  final _lastNameFocusNode = FocusNode();
  final _carTypeFocusNode = FocusNode();
  final String _carTypeFormName = "carTypeFormName";
  final _rangeFocusNode = FocusNode();
  final String _rangeFormName = "rangeFormName";
  final _yearsFocusNode = FocusNode();
  final String _yearsFormName = "yearsFormName";
  final _carYearsFocusNode = FocusNode();
  final String _carYearsFormName = "carYearsFormName";

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: SizedBox(
          height: MediaQuery.of(context).size.height -
              MediaQuery.of(context).padding.vertical,
          child: Column(
            children: [
              const GeneralAppBar(),
              SizedBox(
                height: MediaQuery.of(context).size.height -
                    55 -
                    MediaQuery.of(context).padding.vertical,
                child: Stack(
                  children: [
                    SingleChildScrollView(
                      physics: const ClampingScrollPhysics(),
                      child: FormBuilder(
                        autovalidateMode: AutovalidateMode.disabled,
                        key: _formKey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            const SizedBox(height: 16),
                            Text(
                              "Редактирование профиля",
                              style: AppTextStyles.regular20,
                            ),
                            const SizedBox(height: 24),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 24),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "Пользователь",
                                  style: AppTextStyles.regular20,
                                ),
                              ),
                            ),
                            _buildNameTextField(),
                            _buildLastNameTextField(),
                            _buildYearsTextField(),
                            const SizedBox(height: 24),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 24),
                              child: Align(
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  "Автомобиль",
                                  style: AppTextStyles.regular20,
                                ),
                              ),
                            ),
                            _buildCarNameTextField(),
                            _buildRangeTextField(),
                            _buildCarYearsTextField()
                          ],
                        ),
                      ),
                    ),
                    _buildSaveButton(),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildNameTextField() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 16),
          Text(
            "Введите новое имя:",
            style: AppTextStyles.regular16,
          ),
          const SizedBox(height: 6),
          Container(
            color: AppColors.grey,
            child: GeneralTextField(
              contentPadding:
                  const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
              formName: _nameFormName,
              focusNode: _nameFocusNode,
              formKey: _formKey,
              initialValue: widget.profileModel?.name,
              hintColor: AppColors.textFieldColor,
              hintText: "Имя",
              textStyle: AppTextStyles.regular14.copyWith(
                color: AppColors.textFieldColor,
              ),
              onChanged: (value) {
                _store(context).onChangeName(value);
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildLastNameTextField() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 16),
          Text(
            "Введите новую фамилию:",
            style: AppTextStyles.regular16,
          ),
          const SizedBox(height: 6),
          Container(
            color: AppColors.grey,
            child: GeneralTextField(
              contentPadding:
                  const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
              formName: _lastNameFormName,
              focusNode: _lastNameFocusNode,
              formKey: _formKey,
              initialValue: widget.profileModel?.lastName,
              hintColor: AppColors.textFieldColor,
              hintText: "Фамилия",
              textStyle: AppTextStyles.regular14.copyWith(
                color: AppColors.textFieldColor,
              ),
              onChanged: (value) {
                _store(context).onChangeLastName(value);
                // _loginStore.onChangeVin(value);
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildYearsTextField() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 16),
          Text(
            "Введите новый возраст:",
            style: AppTextStyles.regular16,
          ),
          const SizedBox(height: 6),
          Container(
            color: AppColors.grey,
            child: GeneralTextField(
              contentPadding:
                  const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
              formName: _yearsFormName,
              focusNode: _yearsFocusNode,
              formKey: _formKey,
              initialValue: widget.profileModel?.years.toString(),
              hintColor: AppColors.textFieldColor,
              hintText: "Возраст",
              textStyle: AppTextStyles.regular14.copyWith(
                color: AppColors.textFieldColor,
              ),
              onChanged: (value) {
                _store(context).onChangeUserYears(value);
                // _loginStore.onChangeVin(value);
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildCarNameTextField() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 16),
          Text(
            "Введите название машины:",
            style: AppTextStyles.regular16,
          ),
          const SizedBox(height: 6),
          Container(
            color: AppColors.grey,
            child: GeneralTextField(
              contentPadding:
                  const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
              formName: _carTypeFormName,
              focusNode: _carTypeFocusNode,
              formKey: _formKey,
              initialValue: valueOrEmpty(widget.profileModel?.car?.carType),
              hintColor: AppColors.textFieldColor,
              hintText: "Машина",
              textStyle: AppTextStyles.regular14.copyWith(
                color: AppColors.textFieldColor,
              ),
              onChanged: (value) {
                _store(context).onChangeAutoName(value);
                // _loginStore.onChangeVin(value);
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildRangeTextField() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 16),
          Text(
            "Введите пробег",
            style: AppTextStyles.regular16,
          ),
          const SizedBox(height: 6),
          Container(
            color: AppColors.grey,
            child: GeneralTextField(
              contentPadding:
                  const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
              formName: _rangeFormName,
              focusNode: _rangeFocusNode,
              formKey: _formKey,
              initialValue:
                  valueOrZero(widget.profileModel?.car?.range).toString(),
              hintColor: AppColors.textFieldColor,
              hintText: "Пробег",
              textStyle: AppTextStyles.regular14.copyWith(
                color: AppColors.textFieldColor,
              ),
              onChanged: (value) {
                _store(context).onChangeRange(value);
                // _loginStore.onChangeVin(value);
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildCarYearsTextField() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 16),
          Text(
            "Введите год машины",
            style: AppTextStyles.regular16,
          ),
          const SizedBox(height: 6),
          Container(
            color: AppColors.grey,
            child: GeneralTextField(
              contentPadding:
                  const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
              formName: _carYearsFormName,
              focusNode: _carYearsFocusNode,
              initialValue:
                  valueOrZero(widget.profileModel?.car?.years).toString(),
              formKey: _formKey,
              hintColor: AppColors.textFieldColor,
              hintText: "Год авто",
              textStyle: AppTextStyles.regular14.copyWith(
                color: AppColors.textFieldColor,
              ),
              onChanged: (value) {
                _store(context).onChangeCarYears(value);
                // _loginStore.onChangeVin(value);
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildSaveButton() {
    return Positioned(
      bottom: 24,
      left: 24,
      right: 24,
      child: GeneralButtonWidget(
        borderRadius: 10,
        text: "Сохранить",
        minWidth: double.infinity,
        onTap: () {
          _store(context).saveTap();
          Navigator.of(context).pop();
        },
      ),
    );
  }
}
