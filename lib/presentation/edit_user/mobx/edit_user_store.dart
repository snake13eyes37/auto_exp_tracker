import 'package:auto_exp_tracker/core/enviroment/environment.dart';
import 'package:auto_exp_tracker/core/extensions/extensions.dart';
import 'package:auto_exp_tracker/domain/profile/models/profile_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dart_extensions/dart_extensions.dart';
import 'package:mobx/mobx.dart';

part 'edit_user_store.g.dart';

class EditUserStore = EditUserStoreBase with _$EditUserStore;

abstract class EditUserStoreBase with Store {
  @observable
  String name = "";
  @observable
  String lastName = "";
  @observable
  String userYears = "";
  @observable
  String nameAuto = "";
  @observable
  String range = "";
  @observable
  String carYears = "";
  @observable
  ProfileModel? profileModel;
  @observable
  bool isSave = false;

  @action
  void onChangeName(String? value) {
    name = valueOrEmpty(value);
  }

  @action
  void onChangeLastName(String? value) {
    lastName = valueOrEmpty(value);
  }

  @action
  void onChangeUserYears(String? value) {
    userYears = valueOrEmpty(value);
  }

  @action
  void onChangeAutoName(String? value) {
    nameAuto = valueOrEmpty(value);
  }

  @action
  void onChangeRange(String? value) {
    range = valueOrEmpty(value);
  }

  @action
  void onChangeCarYears(String? value) {
    carYears = valueOrEmpty(value);
  }

  @action
  void onInit() {
    initProfile();
  }

  void initProfile() async {
    try {
      final firebaseInstant = FirebaseFirestore.instance;
      final userID = Environment.vin;
      final result = await firebaseInstant.collection("user").doc(userID).get();
      final data = result.data();
      if (data != null) {
        profileModel = ProfileModel.fromMap(data);
        final tempProfile = profileModel;
        if (tempProfile != null) {
          name = valueOrEmpty(tempProfile.name);
          lastName = valueOrEmpty(tempProfile.lastName);
          userYears = valueOrZero(tempProfile.years).toString();
          nameAuto = valueOrEmpty(tempProfile.car?.carType);
          range = valueOrZero(tempProfile.car?.range).toString();
          carYears = valueOrZero(tempProfile.car?.years).toString();
        }
      }
    } catch (e) {
      //todo error
    }
  }

  @action
  void saveTap() {
    print("was tapped");
    save();
  }

  void save() async {
    final firebaseInstant = FirebaseFirestore.instance;
    final id = Environment.vin;
    final tempProfile = profileModel;
    print(tempProfile);
    if (tempProfile != null) {
      final newCar = tempProfile.car?.copyWith(
        carType: nameAuto.isEmpty ? null : nameAuto,
        years: carYears.toIntOrNull(),
        range: range.toIntOrNull(),
      );
      final updateProfile = tempProfile.copyWith(
        name: name.isEmpty ? null : name,
        lastName: lastName.isEmpty ? null : lastName,
        years: userYears.toIntOrNull(),
        car: newCar,
      );
      print(updateProfile.toMap().toString());
      final result = await firebaseInstant
          .collection("user")
          .doc(id)
          .update(updateProfile.toMap());
      isSave = true;
    }
  }
}
