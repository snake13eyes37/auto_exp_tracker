// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'edit_user_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$EditUserStore on EditUserStoreBase, Store {
  late final _$nameAtom =
      Atom(name: 'EditUserStoreBase.name', context: context);

  @override
  String get name {
    _$nameAtom.reportRead();
    return super.name;
  }

  @override
  set name(String value) {
    _$nameAtom.reportWrite(value, super.name, () {
      super.name = value;
    });
  }

  late final _$lastNameAtom =
      Atom(name: 'EditUserStoreBase.lastName', context: context);

  @override
  String get lastName {
    _$lastNameAtom.reportRead();
    return super.lastName;
  }

  @override
  set lastName(String value) {
    _$lastNameAtom.reportWrite(value, super.lastName, () {
      super.lastName = value;
    });
  }

  late final _$userYearsAtom =
      Atom(name: 'EditUserStoreBase.userYears', context: context);

  @override
  String get userYears {
    _$userYearsAtom.reportRead();
    return super.userYears;
  }

  @override
  set userYears(String value) {
    _$userYearsAtom.reportWrite(value, super.userYears, () {
      super.userYears = value;
    });
  }

  late final _$nameAutoAtom =
      Atom(name: 'EditUserStoreBase.nameAuto', context: context);

  @override
  String get nameAuto {
    _$nameAutoAtom.reportRead();
    return super.nameAuto;
  }

  @override
  set nameAuto(String value) {
    _$nameAutoAtom.reportWrite(value, super.nameAuto, () {
      super.nameAuto = value;
    });
  }

  late final _$rangeAtom =
      Atom(name: 'EditUserStoreBase.range', context: context);

  @override
  String get range {
    _$rangeAtom.reportRead();
    return super.range;
  }

  @override
  set range(String value) {
    _$rangeAtom.reportWrite(value, super.range, () {
      super.range = value;
    });
  }

  late final _$carYearsAtom =
      Atom(name: 'EditUserStoreBase.carYears', context: context);

  @override
  String get carYears {
    _$carYearsAtom.reportRead();
    return super.carYears;
  }

  @override
  set carYears(String value) {
    _$carYearsAtom.reportWrite(value, super.carYears, () {
      super.carYears = value;
    });
  }

  late final _$profileModelAtom =
      Atom(name: 'EditUserStoreBase.profileModel', context: context);

  @override
  ProfileModel? get profileModel {
    _$profileModelAtom.reportRead();
    return super.profileModel;
  }

  @override
  set profileModel(ProfileModel? value) {
    _$profileModelAtom.reportWrite(value, super.profileModel, () {
      super.profileModel = value;
    });
  }

  late final _$isSaveAtom =
      Atom(name: 'EditUserStoreBase.isSave', context: context);

  @override
  bool get isSave {
    _$isSaveAtom.reportRead();
    return super.isSave;
  }

  @override
  set isSave(bool value) {
    _$isSaveAtom.reportWrite(value, super.isSave, () {
      super.isSave = value;
    });
  }

  late final _$EditUserStoreBaseActionController =
      ActionController(name: 'EditUserStoreBase', context: context);

  @override
  void onChangeName(String? value) {
    final _$actionInfo = _$EditUserStoreBaseActionController.startAction(
        name: 'EditUserStoreBase.onChangeName');
    try {
      return super.onChangeName(value);
    } finally {
      _$EditUserStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void onChangeLastName(String? value) {
    final _$actionInfo = _$EditUserStoreBaseActionController.startAction(
        name: 'EditUserStoreBase.onChangeLastName');
    try {
      return super.onChangeLastName(value);
    } finally {
      _$EditUserStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void onChangeUserYears(String? value) {
    final _$actionInfo = _$EditUserStoreBaseActionController.startAction(
        name: 'EditUserStoreBase.onChangeUserYears');
    try {
      return super.onChangeUserYears(value);
    } finally {
      _$EditUserStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void onChangeAutoName(String? value) {
    final _$actionInfo = _$EditUserStoreBaseActionController.startAction(
        name: 'EditUserStoreBase.onChangeAutoName');
    try {
      return super.onChangeAutoName(value);
    } finally {
      _$EditUserStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void onChangeRange(String? value) {
    final _$actionInfo = _$EditUserStoreBaseActionController.startAction(
        name: 'EditUserStoreBase.onChangeRange');
    try {
      return super.onChangeRange(value);
    } finally {
      _$EditUserStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void onChangeCarYears(String? value) {
    final _$actionInfo = _$EditUserStoreBaseActionController.startAction(
        name: 'EditUserStoreBase.onChangeCarYears');
    try {
      return super.onChangeCarYears(value);
    } finally {
      _$EditUserStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void onInit() {
    final _$actionInfo = _$EditUserStoreBaseActionController.startAction(
        name: 'EditUserStoreBase.onInit');
    try {
      return super.onInit();
    } finally {
      _$EditUserStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void saveTap() {
    final _$actionInfo = _$EditUserStoreBaseActionController.startAction(
        name: 'EditUserStoreBase.saveTap');
    try {
      return super.saveTap();
    } finally {
      _$EditUserStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
name: ${name},
lastName: ${lastName},
userYears: ${userYears},
nameAuto: ${nameAuto},
range: ${range},
carYears: ${carYears},
profileModel: ${profileModel},
isSave: ${isSave}
    ''';
  }
}
