import 'package:auto_exp_tracker/core/extensions/extensions.dart';
import 'package:auto_exp_tracker/domain/expensive/expensive_type.dart';
import 'package:auto_exp_tracker/domain/model/expensive_model.dart';
import 'package:auto_exp_tracker/domain/model/fuel_type_enum.dart';
import 'package:auto_exp_tracker/presentation/exp_detail/store/exp_detail_store.dart';
import 'package:auto_exp_tracker/presentation/profile/profile_page.dart';
import 'package:auto_exp_tracker/ui/core/colors.dart';
import 'package:auto_exp_tracker/ui/core/text_styles.dart';
import 'package:auto_exp_tracker/ui/widgets/general_app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';

ExpDetailStore _store(context) =>
    Provider.of<ExpDetailStore>(context, listen: false);

class ExpDetailPage extends StatelessWidget {
  final ExpensiveModel expensiveModel;

  const ExpDetailPage({
    Key? key,
    required this.expensiveModel,
  }) : super(key: key);

  static MaterialPageRoute roure(ExpensiveModel expensiveModel) =>
      MaterialPageRoute(
        builder: (context) => ExpDetailPage(
          expensiveModel: expensiveModel,
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Provider<ExpDetailStore>(
      create: (context) => ExpDetailStore()..init(expensiveModel),
      child: const _ExpDetailScreen(),
    );
  }
}

class _ExpDetailScreen extends StatefulWidget {
  const _ExpDetailScreen({Key? key}) : super(key: key);

  @override
  State<_ExpDetailScreen> createState() => _ExpDetailScreenState();
}

class _ExpDetailScreenState extends State<_ExpDetailScreen> {
  // sdfsdfsd
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Stack(
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height -
                  MediaQuery.of(context).padding.vertical,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  GeneralAppBar(
                    isShowArrow: true,
                    onProfileTap: () {
                      Navigator.of(context).push(ProfilePage.route());
                    },
                  ),
                  _buildChart(context),
                  const SizedBox(height: 24),
                  _buildExpensiveList(context),
                  _buildComment(context),
                ],
              ),
            ),
            _buildBottomButtons(context),
          ],
        ),
      ),
    );
  }

  Widget _buildChart(BuildContext context) {
    return Observer(builder: (context) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const SizedBox(height: 24),
          Text(
            valueOrEmpty(_store(context).expensiveModel?.expName),
            style: AppTextStyles.regular20,
          ),
          const SizedBox(height: 24),
          Text(
            "${valueOrZero(_store(context).expensiveModel?.price)} руб.",
            style: AppTextStyles.regular20,
          ),
        ],
      );
    });
  }

  Widget _buildExpensiveList(BuildContext context) {
    return Observer(
      builder: (context) => Padding(
        padding: const EdgeInsets.all(24),
        child: Container(
          width: double.infinity,
          decoration: BoxDecoration(
            color: AppColors.grey,
            borderRadius: BorderRadius.circular(8),
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 24, horizontal: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                _buildInfo(
                    "Пробег: ${valueOrZero(_store(context).expensiveModel?.kilometers)} км"),
                const SizedBox(height: 15),
                _buildInfo(
                    "Дата: ${valueOrEmpty(_store(context).expensiveModel?.date.formatToDDMMYY)}"),
                _buildFuelInfo(context),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildFuelInfo(BuildContext context) {
    return Observer(builder: (context) {
      if (_store(context).expensiveModel?.type != ExpensiveType.fuel) {
        return const SizedBox.shrink();
      }

      return Column(
        children: [
          const SizedBox(height: 15),
          _buildInfo(
            "Тип: ${valueOrEmpty(_store(context).expensiveModel?.fuelType?.fuelName)}",
          ),
          const SizedBox(height: 15),
          _buildInfo(
            "Литры: ${valueOrZero(_store(context).expensiveModel?.liters)}",
          ),
        ],
      );
    });
  }

  Widget _buildComment(BuildContext context) {
    return Observer(builder: (context) {
      return valueOrEmpty(_store(context).expensiveModel?.description).isEmpty
          ? const SizedBox.shrink()
          : Padding(
              padding: const EdgeInsets.all(24),
              child: Container(
                width: double.infinity,
                decoration: BoxDecoration(
                  color: AppColors.grey,
                  borderRadius: BorderRadius.circular(8),
                ),
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 24, horizontal: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      _buildInfo("Комментарий: "),
                      const SizedBox(height: 15),
                      _buildInfo(
                        valueOrEmpty(
                            _store(context).expensiveModel?.description),
                      ),
                    ],
                  ),
                ),
              ),
            );
    });
  }

  Widget _buildInfo(String text) {
    return Text(
      text,
      style: AppTextStyles.regular15,
    );
  }

  Widget _buildBottomButtons(BuildContext context) {
    return Positioned(
      bottom: 24,
      left: 24,
      right: 24,
      child: _buildButton(
        Icons.delete,
        () {
          _store(context).delete();
          Navigator.of(context).pop();
        },
      ),
    );
  }

  Widget _buildButton(IconData icon, Function onTap) {
    return GestureDetector(
      onTap: () {
        onTap();
        Navigator.of(context).pop();
      },
      child: Container(
        decoration: BoxDecoration(
          color: AppColors.appBarBg,
          borderRadius: BorderRadius.circular(31),
        ),
        child: Padding(
          padding: const EdgeInsets.all(4),
          child: Icon(
            icon,
            size: 49,
          ),
        ),
      ),
    );
  }

// Widget _buildExpensiveElement(
//   BuildContext context,
//   ExpensiveModel expensive,
// ) {
//   String expTitle = expensive.expName;
//   if (expensive.type == ExpensiveType.fuel) {
//     final fuelType = valueOrEmpty(expensive.fuelType?.fuelName);
//     final liters = valueOrZero(expensive.liters);
//     expTitle = "Заправка $fuelType ,$liters л.";
//   }
//   return Container(
//     decoration: BoxDecoration(
//       color: AppColors.darkGrey,
//       borderRadius: BorderRadius.circular(8),
//     ),
//     child: Padding(
//       padding: const EdgeInsets.symmetric(
//         vertical: 14,
//         horizontal: 8,
//       ),
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           Text(
//             expTitle,
//             style: AppTextStyles.regular15,
//           ),
//           const SizedBox(height: 2),
//           Text(
//             "${expensive.kilometers} км. , ${expensive.price} руб.",
//             style: AppTextStyles.regular15,
//           ),
//           const SizedBox(height: 2),
//           Text(
//             "Подробнее...",
//             style: AppTextStyles.regular15,
//           ),
//         ],
//       ),
//     ),
//   );
// }
}
