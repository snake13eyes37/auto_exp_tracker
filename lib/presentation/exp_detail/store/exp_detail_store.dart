import 'package:auto_exp_tracker/core/enviroment/environment.dart';
import 'package:auto_exp_tracker/domain/expensive/expensive_type.dart';
import 'package:auto_exp_tracker/domain/model/expensive_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mobx/mobx.dart';

part 'exp_detail_store.g.dart';

class ExpDetailStore = ExpDetailStoreBase with _$ExpDetailStore;

abstract class ExpDetailStoreBase with Store {
  @observable
  ExpensiveModel? expensiveModel;

  @action
  void init(ExpensiveModel expensiveModel) {
    this.expensiveModel = expensiveModel;
  }


  @action
  void delete() {
    final expModel = expensiveModel;
    if (expModel != null) {
      deleteExp(expModel);
    }
  }

  void deleteExp(ExpensiveModel expensive) async {
    final firebaseInstant = FirebaseFirestore.instance;
    final userID = Environment.vin;
    final result = await firebaseInstant
        .collection("user")
        .doc(userID)
        .get();
    await result.reference
        .collection(expensive.type.toMap)
        .doc(expensive.id)
        .delete();
    print(expensive.id);
  }
}
