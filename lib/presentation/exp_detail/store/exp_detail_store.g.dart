// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'exp_detail_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$ExpDetailStore on ExpDetailStoreBase, Store {
  late final _$expensiveModelAtom =
      Atom(name: 'ExpDetailStoreBase.expensiveModel', context: context);

  @override
  ExpensiveModel? get expensiveModel {
    _$expensiveModelAtom.reportRead();
    return super.expensiveModel;
  }

  @override
  set expensiveModel(ExpensiveModel? value) {
    _$expensiveModelAtom.reportWrite(value, super.expensiveModel, () {
      super.expensiveModel = value;
    });
  }

  late final _$ExpDetailStoreBaseActionController =
      ActionController(name: 'ExpDetailStoreBase', context: context);

  @override
  void init(ExpensiveModel expensiveModel) {
    final _$actionInfo = _$ExpDetailStoreBaseActionController.startAction(
        name: 'ExpDetailStoreBase.init');
    try {
      return super.init(expensiveModel);
    } finally {
      _$ExpDetailStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void delete() {
    final _$actionInfo = _$ExpDetailStoreBaseActionController.startAction(
        name: 'ExpDetailStoreBase.delete');
    try {
      return super.delete();
    } finally {
      _$ExpDetailStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
expensiveModel: ${expensiveModel}
    ''';
  }
}
