import 'package:auto_exp_tracker/core/enviroment/environment.dart';
import 'package:auto_exp_tracker/core/extensions/extensions.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mobx/mobx.dart';

part 'login_store.g.dart';

class LoginStore = LoginStoreBase with _$LoginStore;

abstract class LoginStoreBase with Store {
  @observable
  String vin = "";

  @observable
  String error = "";

  @observable
  bool isNavigate = false;

  @action
  void onChangeVin(String? vin) {
    print("new value: $vin");
    this.vin = valueOrEmpty(vin);
    error = "";
  }

  @action
  void loginTap() {
    final regex = RegExp("^[A-Z0-9]{17}\$");
    // if (!regex.hasMatch(vin.toUpperCase())) {
    //   error =
    //       "Неверный VIN: количество символов должно быть 17, Доступны только латинские символы и цифры";
    // } else {
      error = "";
      loginFirebase();
    //}
  }

  void loginFirebase() async {
    try {
      final firebaseInstant = FirebaseFirestore.instance;
      final result = await firebaseInstant.collection("user").doc(vin).get();
      final data = result.data();
      if (data == null) {
        await firebaseInstant.collection("user").doc(vin).set({"vin": vin});
      }

      Environment.setVin(vin);
      isNavigate = true;
    } catch (e) {
      //todo
    }
  }
}
