// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$LoginStore on LoginStoreBase, Store {
  late final _$vinAtom = Atom(name: 'LoginStoreBase.vin', context: context);

  @override
  String get vin {
    _$vinAtom.reportRead();
    return super.vin;
  }

  @override
  set vin(String value) {
    _$vinAtom.reportWrite(value, super.vin, () {
      super.vin = value;
    });
  }

  late final _$errorAtom = Atom(name: 'LoginStoreBase.error', context: context);

  @override
  String get error {
    _$errorAtom.reportRead();
    return super.error;
  }

  @override
  set error(String value) {
    _$errorAtom.reportWrite(value, super.error, () {
      super.error = value;
    });
  }

  late final _$isNavigateAtom =
      Atom(name: 'LoginStoreBase.isNavigate', context: context);

  @override
  bool get isNavigate {
    _$isNavigateAtom.reportRead();
    return super.isNavigate;
  }

  @override
  set isNavigate(bool value) {
    _$isNavigateAtom.reportWrite(value, super.isNavigate, () {
      super.isNavigate = value;
    });
  }

  late final _$LoginStoreBaseActionController =
      ActionController(name: 'LoginStoreBase', context: context);

  @override
  void onChangeVin(String? vin) {
    final _$actionInfo = _$LoginStoreBaseActionController.startAction(
        name: 'LoginStoreBase.onChangeVin');
    try {
      return super.onChangeVin(vin);
    } finally {
      _$LoginStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void loginTap() {
    final _$actionInfo = _$LoginStoreBaseActionController.startAction(
        name: 'LoginStoreBase.loginTap');
    try {
      return super.loginTap();
    } finally {
      _$LoginStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
vin: ${vin},
error: ${error},
isNavigate: ${isNavigate}
    ''';
  }
}
