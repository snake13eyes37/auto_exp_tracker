import 'package:auto_exp_tracker/presentation/login/mobx/login_store.dart';
import 'package:auto_exp_tracker/presentation/profile/profile_page.dart';
import 'package:auto_exp_tracker/presentation/statistic/statistic_page.dart';
import 'package:auto_exp_tracker/ui/core/colors.dart';
import 'package:auto_exp_tracker/ui/core/text_styles.dart';
import 'package:auto_exp_tracker/ui/widgets/general_button_widget.dart';
import 'package:auto_exp_tracker/ui/widgets/general_text_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:get_it/get_it.dart';
import 'package:mobx/mobx.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const LoginScreen();
  }
}

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late final LoginStore _loginStore;
  final String _fieldFormName = "fieldFormName";
  final _fieldFocusNode = FocusNode();
  final _formKey = GlobalKey<FormBuilderState>();
  final List<ReactionDisposer> disposers = [];

  @override
  void initState() {
    super.initState();
    _loginStore = LoginStore();
    disposers.add(
      when((p0) => _loginStore.isNavigate, () {
        Navigator.of(context).push(StatisticPage.route());
      }),
    );
  }

  @override
  void dispose() {
    super.dispose();
    disposers.forEach((disposer) {
      disposer();
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 36, vertical: 140),
          child: SingleChildScrollView(
            child: FormBuilder(
              key: _formKey,
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    _buildTopBlock(),
                    const SizedBox(height: 100),
                    _buildBottomBlock(),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildTopBlock() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Center(
          child: Text(
            "Добро пожаловать!",
            style: AppTextStyles.regular32,
          ),
        ),
        const SizedBox(height: 80),
        _buildTextField(),
        const SizedBox(height: 6),
        Text(
          "Введите VIN Вашего авто для авторизации",
          style: AppTextStyles.regular24,
          textAlign: TextAlign.center,
        ),
      ],
    );
  }

  Widget _buildTextField() {
    return ClipRRect(
      borderRadius: BorderRadius.circular(8),
      child: Container(
        color: AppColors.grey,
        child: GeneralTextField(
          contentPadding:
              const EdgeInsets.symmetric(horizontal: 18, vertical: 20),
          formName: _fieldFormName,
          focusNode: _fieldFocusNode,
          formKey: _formKey,
          hintColor: AppColors.textFieldColor,
          hintText: "VIN Вашего авто",
          textStyle: AppTextStyles.regular24.copyWith(
            color: AppColors.textFieldColor,
          ),
          onChanged: (value) {
            _loginStore.onChangeVin(value);
          },
        ),
      ),
    );
  }

  Widget _buildBottomBlock() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Observer(
          builder: (_) => _loginStore.error.isNotEmpty
              ? Center(
                  child: Text(
                    _loginStore.error,
                    style: AppTextStyles.regular20.copyWith(
                      color: AppColors.red,
                    ),
                    textAlign: TextAlign.center,
                  ),
                )
              : const SizedBox.shrink(),
        ),
        const SizedBox(height: 36),
        GeneralButtonWidget(
          borderRadius: 10,
          text: "Отслеживать",
          minWidth: double.infinity,
          onTap: () {
            // Navigator.of(context).push(ProfilePage.route());
            _loginStore.loginTap();
          },
        ),
      ],
    );
  }
}
