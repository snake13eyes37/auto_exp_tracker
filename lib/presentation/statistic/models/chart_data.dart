import 'package:flutter/material.dart';

class ChartData {
  final Color color;
  final int totalPrice;

//<editor-fold desc="Data Methods">
  const ChartData({
    required this.color,
    required this.totalPrice,
  });

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      (other is ChartData &&
          runtimeType == other.runtimeType &&
          color == other.color &&
          totalPrice == other.totalPrice);

  @override
  int get hashCode => color.hashCode ^ totalPrice.hashCode;

  @override
  String toString() {
    return 'ChartData{' + ' color: $color,' + ' totalPrice: $totalPrice,' + '}';
  }

  ChartData copyWith({
    Color? color,
    int? totalPrice,
  }) {
    return ChartData(
      color: color ?? this.color,
      totalPrice: totalPrice ?? this.totalPrice,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'color': this.color,
      'totalPrice': this.totalPrice,
    };
  }

  factory ChartData.fromMap(Map<String, dynamic> map) {
    return ChartData(
      color: map['color'] as Color,
      totalPrice: map['totalPrice'] as int,
    );
  }

//</editor-fold>
}
