import 'package:auto_exp_tracker/core/mocks/app_mocks.dart';
import 'package:auto_exp_tracker/domain/expensive/expensive_type.dart';
import 'package:auto_exp_tracker/domain/model/expensive_model.dart';
import 'package:auto_exp_tracker/presentation/create_new_exp/create_new_exp_page.dart';
import 'package:auto_exp_tracker/presentation/exp_type_detail/exp_type_detail_page.dart';
import 'package:auto_exp_tracker/presentation/profile/profile_page.dart';
import 'package:auto_exp_tracker/presentation/statistic/mobx/statistic_store.dart';
import 'package:auto_exp_tracker/presentation/statistic/models/chart_data.dart';
import 'package:auto_exp_tracker/ui/core/colors.dart';
import 'package:auto_exp_tracker/ui/core/text_styles.dart';
import 'package:auto_exp_tracker/ui/widgets/general_app_bar.dart';
import 'package:auto_exp_tracker/ui/widgets/general_button_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

StatisticStore _store(context) =>
    Provider.of<StatisticStore>(context, listen: false);

class StatisticPage extends StatelessWidget {
  const StatisticPage({Key? key}) : super(key: key);

  static MaterialPageRoute route() => MaterialPageRoute(
        builder: (context) => const StatisticPage(),
      );

  @override
  Widget build(BuildContext context) {
    return Provider<StatisticStore>(
      create: (context) => StatisticStore()..init(),
      builder: (context, _) => const _StatisticScreen(),
    );
    ;
  }
}

class _StatisticScreen extends StatefulWidget {
  const _StatisticScreen({Key? key}) : super(key: key);

  @override
  State<_StatisticScreen> createState() => _StatisticScreenState();
}

class _StatisticScreenState extends State<_StatisticScreen> {
  // final list = [
  //   const ChartData(
  //     color: AppColors.fuelColor,
  //     totalPrice: 200,
  //   ),
  //   const ChartData(
  //     color: AppColors.consumablesColor,
  //     totalPrice: 150,
  //   ),
  //   const ChartData(
  //     color: AppColors.changeDetailColor,
  //     totalPrice: 300,
  //   ),
  //   const ChartData(
  //     color: AppColors.technicalPairColor,
  //     totalPrice: 250,
  //   ),
  //   const ChartData(
  //     color: AppColors.otherColor,
  //     totalPrice: 50,
  //   ),
  // ];

  int getTotalPriceFromType(BuildContext context, ExpensiveType type) {
    final int totalPrice;
    switch (type) {
      case ExpensiveType.fuel:
        totalPrice = _store(context).fuelTotalPrice;
        break;
      case ExpensiveType.consumables:
        totalPrice = _store(context).consumablesTotalPrice;
        break;
      case ExpensiveType.service:
        totalPrice = _store(context).serviceTotalPrice;
        break;
      case ExpensiveType.changeDetails:
        totalPrice = _store(context).changeDetailsTotalPrice;
        break;
      case ExpensiveType.other:
        totalPrice = _store(context).otherTotalPrice;
        break;
    }
    return totalPrice;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height -
              MediaQuery.of(context).padding.vertical,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              GeneralAppBar(
                isShowArrow: false,
                onProfileTap: () {
                  Navigator.of(context).push(ProfilePage.route());
                },
              ),
              _buildChart(context),
              const SizedBox(height: 24),
              Observer(builder: (context) {
                if (_store(context).isEmptyExpensive) {
                  return Text(
                    "Извините у вас нет затрат!!!",
                    style: AppTextStyles.bold20,
                  );
                }
                return Column(
                  children: _store(context)
                      .lastExpensiveList
                      .map((e) => _buildExpensiveElement(context, e))
                      .toList(),
                );
              }),
              Padding(
                padding: const EdgeInsets.symmetric(
                  vertical: 24,
                  horizontal: 24,
                ),
                child: GeneralButtonWidget(
                  borderRadius: 75,
                  text: "Создать затрату",
                  minWidth: double.infinity,
                  onTap: () async {
                    await Navigator.of(context).push(CreateNewExpPage.route());
                    _store(context).init();
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildChart(BuildContext context) {
    return Observer(builder: (context) {
      final chartData = _store(context)
          .lastExpensiveList
          .map(
            (e) => ChartData(
              color: e.type.color,
              totalPrice: getTotalPriceFromType(context, e.type),
            ),
          )
          .toList();
      final int totalPrice = _store(context).allPrice;

      return SizedBox(
        width: 283,
        height: 283,
        child: Stack(
          children: [
            SfCircularChart(
              margin: EdgeInsets.zero,
              series: <PieSeries<ChartData, String>>[
                PieSeries<ChartData, String>(
                  explode: false,
                  explodeIndex: 0,
                  dataSource: chartData,
                  xValueMapper: (data, _) => "",
                  yValueMapper: (data, _) => data.totalPrice,
                  pointColorMapper: (data, _) => data.color,
                  dataLabelSettings: const DataLabelSettings(isVisible: false),
                ),
              ],
            ),
            Align(
              alignment: Alignment.center,
              child: Container(
                height: 100,
                width: 100,
                decoration: BoxDecoration(
                  color: AppColors.white,
                  borderRadius: BorderRadius.circular(75),
                ),
                child: Center(
                  child: Text(
                    "$totalPrice руб",
                    style: AppTextStyles.regular16,
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    });
  }

  Widget _buildExpensiveElement(
    BuildContext context,
    ExpensiveModel expensive,
  ) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 4),
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).push(ExpTypeDetailPage.route(expensive.type));
        },
        child: ClipRRect(
          borderRadius: BorderRadius.circular(10),
          child: Container(
            decoration: BoxDecoration(
              color: AppColors.grey,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Row(
              children: [
                Container(
                  height: 64,
                  width: 19,
                  color: expensive.type.color,
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 12.0, horizontal: 8),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Observer(builder: (context) {
                        final totalPrice =
                            getTotalPriceFromType(context, expensive.type);
                        return Text(
                          "${expensive.type.title} ($totalPrice руб)",
                          style: AppTextStyles.regular16,
                        );
                      }),
                      const SizedBox(height: 4),
                      Text(
                        "Последняя затрата: ${expensive.expName} ,${expensive.price} руб",
                        style: AppTextStyles.regular14,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

// class _PieData {
//   _PieData(this.xData, this.yData, {this.text = ""});
//
//   final String xData;
//   final num;
//
//   final String text;
// }
