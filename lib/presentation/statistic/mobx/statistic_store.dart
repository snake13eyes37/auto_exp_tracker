import 'package:auto_exp_tracker/core/enviroment/environment.dart';
import 'package:auto_exp_tracker/core/extensions/extensions.dart';
import 'package:auto_exp_tracker/domain/model/expensive_model.dart';
import 'package:auto_exp_tracker/domain/profile/models/profile_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:mobx/mobx.dart';

part 'statistic_store.g.dart';

class StatisticStore = StatisticStoreBase with _$StatisticStore;

abstract class StatisticStoreBase with Store {
  @observable
  List<ExpensiveModel> fuel = [];
  @observable
  List<ExpensiveModel> consumables = [];
  @observable
  List<ExpensiveModel> service = [];
  @observable
  List<ExpensiveModel> changeDetail = [];
  @observable
  List<ExpensiveModel> other = [];

  @computed
  int get allPrice =>
      fuelTotalPrice +
      consumablesTotalPrice +
      serviceTotalPrice +
      changeDetailsTotalPrice +
      otherTotalPrice;

  @computed
  List<ExpensiveModel> get lastExpensiveList {
    final List<ExpensiveModel> list = [];
    if (fuel.isNotEmpty) {
      list.add(fuel.first);
    }
    if (consumables.isNotEmpty) {
      list.add(consumables.first);
    }
    if (service.isNotEmpty) {
      list.add(service.first);
    }
    if (changeDetail.isNotEmpty) {
      list.add(changeDetail.first);
    }
    if (other.isNotEmpty) {
      list.add(other.first);
    }

    return list;
  }

  @computed
  int get fuelTotalPrice =>
      fuel.fold<int>(0, (pv, ExpensiveModel e) => pv + e.price);

  @computed
  int get consumablesTotalPrice =>
      consumables.fold<int>(0, (pv, ExpensiveModel e) => pv + e.price);

  @computed
  int get serviceTotalPrice =>
      service.fold<int>(0, (pv, ExpensiveModel e) => pv + e.price);

  @computed
  int get changeDetailsTotalPrice =>
      changeDetail.fold<int>(0, (pv, ExpensiveModel e) => pv + e.price);

  @computed
  int get otherTotalPrice =>
      other.fold<int>(0, (pv, ExpensiveModel e) => pv + e.price);

  @computed
  bool get isEmptyExpensive =>
      fuel.isEmpty &&
      consumables.isEmpty &&
      service.isEmpty &&
      changeDetail.isEmpty &&
      other.isEmpty;

  @action
  void init() {
    initProfile();
  }

  void initProfile() async {
    final firebaseInstant = FirebaseFirestore.instance;
    final userID = Environment.vin;
    final result = await firebaseInstant.collection("user").doc(userID).get();
    final fuel = await result.reference.collection("fuel").get();
    this.fuel = mapExpensiveList(fuel);
    final consumables = await result.reference.collection("consumables").get();
    this.consumables = mapExpensiveList(consumables);
    final service = await result.reference.collection("service").get();
    this.service = mapExpensiveList(service);
    final changeDetail =
        await result.reference.collection("changeDetails").get();
    this.changeDetail = mapExpensiveList(changeDetail);
    final other = await result.reference.collection("other").get();
    this.other = mapExpensiveList(other);
  }

  List<ExpensiveModel> mapExpensiveList(
    QuerySnapshot<Map<String, dynamic>> snapshot,
  ) {
    final list = snapshot.docs
        .map((e) => ExpensiveModel.fromMap(e.data(), e.id))
        .toList()
        .reversed
        .toList()
      ..sort((a, b) => a.date.compareTo(b.date));
    return list;
  }
}
