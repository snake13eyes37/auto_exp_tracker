// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'statistic_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$StatisticStore on StatisticStoreBase, Store {
  Computed<int>? _$allPriceComputed;

  @override
  int get allPrice =>
      (_$allPriceComputed ??= Computed<int>(() => super.allPrice,
              name: 'StatisticStoreBase.allPrice'))
          .value;
  Computed<List<ExpensiveModel>>? _$lastExpensiveListComputed;

  @override
  List<ExpensiveModel> get lastExpensiveList => (_$lastExpensiveListComputed ??=
          Computed<List<ExpensiveModel>>(() => super.lastExpensiveList,
              name: 'StatisticStoreBase.lastExpensiveList'))
      .value;
  Computed<int>? _$fuelTotalPriceComputed;

  @override
  int get fuelTotalPrice =>
      (_$fuelTotalPriceComputed ??= Computed<int>(() => super.fuelTotalPrice,
              name: 'StatisticStoreBase.fuelTotalPrice'))
          .value;
  Computed<int>? _$consumablesTotalPriceComputed;

  @override
  int get consumablesTotalPrice => (_$consumablesTotalPriceComputed ??=
          Computed<int>(() => super.consumablesTotalPrice,
              name: 'StatisticStoreBase.consumablesTotalPrice'))
      .value;
  Computed<int>? _$serviceTotalPriceComputed;

  @override
  int get serviceTotalPrice => (_$serviceTotalPriceComputed ??= Computed<int>(
          () => super.serviceTotalPrice,
          name: 'StatisticStoreBase.serviceTotalPrice'))
      .value;
  Computed<int>? _$changeDetailsTotalPriceComputed;

  @override
  int get changeDetailsTotalPrice => (_$changeDetailsTotalPriceComputed ??=
          Computed<int>(() => super.changeDetailsTotalPrice,
              name: 'StatisticStoreBase.changeDetailsTotalPrice'))
      .value;
  Computed<int>? _$otherTotalPriceComputed;

  @override
  int get otherTotalPrice =>
      (_$otherTotalPriceComputed ??= Computed<int>(() => super.otherTotalPrice,
              name: 'StatisticStoreBase.otherTotalPrice'))
          .value;

  late final _$fuelAtom =
      Atom(name: 'StatisticStoreBase.fuel', context: context);

  @override
  List<ExpensiveModel> get fuel {
    _$fuelAtom.reportRead();
    return super.fuel;
  }

  @override
  set fuel(List<ExpensiveModel> value) {
    _$fuelAtom.reportWrite(value, super.fuel, () {
      super.fuel = value;
    });
  }

  late final _$consumablesAtom =
      Atom(name: 'StatisticStoreBase.consumables', context: context);

  @override
  List<ExpensiveModel> get consumables {
    _$consumablesAtom.reportRead();
    return super.consumables;
  }

  @override
  set consumables(List<ExpensiveModel> value) {
    _$consumablesAtom.reportWrite(value, super.consumables, () {
      super.consumables = value;
    });
  }

  late final _$serviceAtom =
      Atom(name: 'StatisticStoreBase.service', context: context);

  @override
  List<ExpensiveModel> get service {
    _$serviceAtom.reportRead();
    return super.service;
  }

  @override
  set service(List<ExpensiveModel> value) {
    _$serviceAtom.reportWrite(value, super.service, () {
      super.service = value;
    });
  }

  late final _$changeDetailAtom =
      Atom(name: 'StatisticStoreBase.changeDetail', context: context);

  @override
  List<ExpensiveModel> get changeDetail {
    _$changeDetailAtom.reportRead();
    return super.changeDetail;
  }

  @override
  set changeDetail(List<ExpensiveModel> value) {
    _$changeDetailAtom.reportWrite(value, super.changeDetail, () {
      super.changeDetail = value;
    });
  }

  late final _$otherAtom =
      Atom(name: 'StatisticStoreBase.other', context: context);

  @override
  List<ExpensiveModel> get other {
    _$otherAtom.reportRead();
    return super.other;
  }

  @override
  set other(List<ExpensiveModel> value) {
    _$otherAtom.reportWrite(value, super.other, () {
      super.other = value;
    });
  }

  late final _$StatisticStoreBaseActionController =
      ActionController(name: 'StatisticStoreBase', context: context);

  @override
  void init() {
    final _$actionInfo = _$StatisticStoreBaseActionController.startAction(
        name: 'StatisticStoreBase.init');
    try {
      return super.init();
    } finally {
      _$StatisticStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
fuel: ${fuel},
consumables: ${consumables},
service: ${service},
changeDetail: ${changeDetail},
other: ${other},
allPrice: ${allPrice},
lastExpensiveList: ${lastExpensiveList},
fuelTotalPrice: ${fuelTotalPrice},
consumablesTotalPrice: ${consumablesTotalPrice},
serviceTotalPrice: ${serviceTotalPrice},
changeDetailsTotalPrice: ${changeDetailsTotalPrice},
otherTotalPrice: ${otherTotalPrice}
    ''';
  }
}
