import 'package:auto_exp_tracker/core/extensions/date_time_extensions.dart';
import 'package:auto_exp_tracker/core/extensions/value_extensions.dart';
import 'package:auto_exp_tracker/domain/expensive/expensive_type.dart';
import 'package:auto_exp_tracker/domain/model/expensive_model.dart';
import 'package:auto_exp_tracker/domain/model/fuel_type_enum.dart';
import 'package:auto_exp_tracker/domain/profile/models/profile_model.dart';
import 'package:auto_exp_tracker/presentation/create_new_exp/expensive_type_picker.dart';
import 'package:auto_exp_tracker/presentation/create_new_exp/fuel_type_picker.dart';
import 'package:auto_exp_tracker/presentation/create_new_exp/store/create_new_exp_store.dart';
import 'package:auto_exp_tracker/presentation/edit_user/mobx/edit_user_store.dart';
import 'package:auto_exp_tracker/ui/core/colors.dart';
import 'package:auto_exp_tracker/ui/core/text_styles.dart';
import 'package:auto_exp_tracker/ui/utils/input_formatters.dart';
import 'package:auto_exp_tracker/ui/utils/validators.dart';
import 'package:auto_exp_tracker/ui/widgets/bottom_sheet_fullscreen_dialog.dart';
import 'package:auto_exp_tracker/ui/widgets/general_app_bar.dart';
import 'package:auto_exp_tracker/ui/widgets/general_button_widget.dart';
import 'package:auto_exp_tracker/ui/widgets/general_text_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:provider/provider.dart';

CreateNewExpStore _store(context) =>
    Provider.of<CreateNewExpStore>(context, listen: false);

class CreateNewExpPage extends StatelessWidget {
  final ExpensiveModel? expensiveModel;

  const CreateNewExpPage({
    Key? key,
    this.expensiveModel,
  }) : super(key: key);

  static MaterialPageRoute route({ExpensiveModel? expensiveModel}) =>
      MaterialPageRoute(
        builder: (context) => CreateNewExpPage(expensiveModel: expensiveModel),
      );

  @override
  Widget build(BuildContext context) {
    return Provider<CreateNewExpStore>(
      create: (context) => CreateNewExpStore(),
      builder: (context, _) =>
          _CreateNewExpScreen(expensiveModel: expensiveModel),
    );
  }
}

class _CreateNewExpScreen extends StatefulWidget {
  final ExpensiveModel? expensiveModel;

  const _CreateNewExpScreen({
    Key? key,
    required this.expensiveModel,
  }) : super(key: key);

  @override
  State<_CreateNewExpScreen> createState() => _CreateNewExpScreenState();
}

class _CreateNewExpScreenState extends State<_CreateNewExpScreen> {
  final _formKey = GlobalKey<FormBuilderState>();
  final String _nameFormName = "nameFormName";
  final _nameFocusNode = FocusNode();
  final String _litersFormName = "litersFormName";
  final _litersFocusNode = FocusNode();
  final _priceFocusNode = FocusNode();
  final String _priceFormName = "priceFormName";
  final _rangeFocusNode = FocusNode();
  final String _rangeFormName = "rangeFormName";
  final _descriptionFocusNode = FocusNode();
  final String _descriptionFormName = "descriptionFormName";
  final _expTypeFocusNode = FocusNode();
  final String _expTypeFormName = "expTypeFormName";
  final _dateFocusNode = FocusNode();
  final String _dateFormName = "dateFormName";
  final _fuelTypeFocusNode = FocusNode();
  final String _fuelTypeFormName = "fuelTypeFormName";
  late final MaskTextInputFormatter _dateFormatter;
  final TextEditingController _dateController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _dateFormatter = InputFormatters.dateFormatter(
      initialValue: widget.expensiveModel?.date.formatToDDMMYYYY,
    );
    _dateController.text = _dateFormatter.getMaskedText();
  }

  void _openCountryPicker(BuildContext context) async {
    FocusScope.of(context).unfocus();
    await showFullScreenBottomSheetDialog(
      context,
      ExpensiveTypePickerPage(
        lastSelectedType: _store(context).expType,
        onGenderType: (expType) {
          _store(context).onChangeExpType(expType);
        },
      ),
      paddingTop: MediaQuery.of(context).size.height / 2,
    );
  }

  void _openFuelTypePicker(BuildContext context) async {
    FocusScope.of(context).unfocus();
    await showFullScreenBottomSheetDialog(
      context,
      FuelTypePickerPage(
        lastSelectedType: _store(context).fuelType,
        onGenderType: (expType) {
          _store(context).onChangeFuelType(expType);
        },
      ),
      paddingTop: MediaQuery.of(context).size.height / 2,
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: SizedBox(
          height: MediaQuery.of(context).size.height -
              MediaQuery.of(context).padding.vertical,
          child: Column(
            children: [
              const GeneralAppBar(),
              SizedBox(
                height: MediaQuery.of(context).size.height -
                    55 -
                    MediaQuery.of(context).padding.vertical,
                child: Stack(
                  children: [
                    SingleChildScrollView(
                      physics: const ClampingScrollPhysics(),
                      child: FormBuilder(
                        autovalidateMode: AutovalidateMode.disabled,
                        key: _formKey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            const SizedBox(height: 16),
                            _buildCountryField(context),
                            _buildNameTextField(context),
                            _buildDateField(context),
                            _buildRangeTextField(context),
                            _buildPriceTextField(context),
                            _buildFuelTypeInfo(context),
                            _buildDescriptionTextField(context),

                            //  _buildCarYearsTextField()
                          ],
                        ),
                      ),
                    ),
                    _buildSaveButton(context),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildNameTextField(BuildContext context) {
    return Observer(builder: (context) {
      if (_store(context).expType == ExpensiveType.fuel) {
        return const SizedBox.shrink();
      }
      return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const SizedBox(height: 16),
            Text(
              "Введите название:",
              style: AppTextStyles.regular16,
            ),
            const SizedBox(height: 6),
            Container(
              color: AppColors.grey,
              child: GeneralTextField(
                contentPadding:
                    const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                formName: _nameFormName,
                focusNode: _nameFocusNode,
                formKey: _formKey,
                initialValue: widget.expensiveModel?.expName,
                hintColor: AppColors.textFieldColor,
                hintText: "Имя",
                textStyle: AppTextStyles.regular14.copyWith(
                  color: AppColors.textFieldColor,
                ),
                onChanged: (value) {
                  _store(context).onChangeName(value);
                },
              ),
            ),
          ],
        ),
      );
    });
  }

  Widget _buildLitersTextField(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Литры:",
          style: AppTextStyles.regular16,
        ),
        const SizedBox(height: 6),
        Container(
          color: AppColors.grey,
          child: GeneralTextField(
            contentPadding:
                const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
            formName: _litersFormName,
            focusNode: _litersFocusNode,
            formKey: _formKey,
            validator: FormBuilderValidators.integer(
              errorText: "Некорректные данные",
            ),
            initialValue: widget.expensiveModel?.liters.toString(),
            hintColor: AppColors.textFieldColor,
            hintText: "00",
            textStyle: AppTextStyles.regular14.copyWith(
              color: AppColors.textFieldColor,
            ),
            keyboardType: TextInputType.number,
            onChanged: (value) {
              _store(context).onChangeLiters(value);
            },
          ),
        ),
      ],
    );
  }

  Widget _buildPriceTextField(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 16),
          Text(
            "Введите цену:",
            style: AppTextStyles.regular16,
          ),
          const SizedBox(height: 6),
          Container(
            color: AppColors.grey,
            child: GeneralTextField(
              contentPadding:
                  const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
              formName: _priceFormName,
              focusNode: _priceFocusNode,
              formKey: _formKey,
              validator: FormBuilderValidators.integer(
                errorText: "Некорректные данные",
              ),
              initialValue: widget.expensiveModel?.price.toString(),
              hintColor: AppColors.textFieldColor,
              hintText: "0р",
              keyboardType: TextInputType.number,
              textStyle: AppTextStyles.regular14.copyWith(
                color: AppColors.textFieldColor,
              ),
              onChanged: (value) {
                _store(context).onChangePrice(value);
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildDescriptionTextField(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 16),
          Text(
            "Описание:",
            style: AppTextStyles.regular16,
          ),
          const SizedBox(height: 6),
          Container(
            color: AppColors.grey,
            child: GeneralTextField(
              contentPadding:
                  const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
              formName: _descriptionFormName,
              focusNode: _descriptionFocusNode,
              formKey: _formKey,
              initialValue: widget.expensiveModel?.description,
              hintColor: AppColors.textFieldColor,
              hintText: "Заправка Роснефть на Белинского",
              textStyle: AppTextStyles.regular14.copyWith(
                color: AppColors.textFieldColor,
              ),
              minLines: 10,
              maxLines: 10,
              onChanged: (value) {
                _store(context).onChangeDescription(value);
                // _loginStore.onChangeVin(value);
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildRangeTextField(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 16),
          Text(
            "Введите километраж:",
            style: AppTextStyles.regular16,
          ),
          const SizedBox(height: 6),
          Container(
            color: AppColors.grey,
            child: GeneralTextField(
              contentPadding:
                  const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
              formName: _rangeFormName,
              focusNode: _rangeFocusNode,
              formKey: _formKey,
              keyboardType: TextInputType.number,
              validator: FormBuilderValidators.integer(
                errorText: "Некорректные данные",
              ),
              initialValue: widget.expensiveModel?.kilometers.toString(),
              hintColor: AppColors.textFieldColor,
              hintText: "0000 км",
              textStyle: AppTextStyles.regular14.copyWith(
                color: AppColors.textFieldColor,
              ),
              onChanged: (value) {
                _store(context).onChangeRange(value);
                // _loginStore.onChangeVin(value);
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildCountryField(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 16),
          Text(
            "Выберите тип:",
            style: AppTextStyles.regular16,
          ),
          const SizedBox(height: 6),
          Container(
            color: AppColors.grey,
            child: GestureDetector(
              onTap: () {
                _openCountryPicker(context);
              },
              child: Observer(
                builder: (context) => GeneralTextField(
                  contentPadding: const EdgeInsets.only(left: 8, top: 12),
                  formName: _expTypeFormName,
                  formKey: _formKey,
                  focusNode: _expTypeFocusNode,
                  enabled: false,
                  hintText: _store(context).expType.title,
                  hintColor: AppColors.textFieldColor,
                  suffixIcon: const Icon(
                    Icons.expand_more_rounded,
                    size: 28,
                    color: AppColors.black,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildFuelTypeInfo(BuildContext context) {
    return Observer(builder: (context) {
      if (_store(context).expType != ExpensiveType.fuel) {
        return const SizedBox.shrink();
      }
      return Padding(
        padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 24),
        child: Row(
          children: [
            Expanded(child: _buildFuelTypeField(context)),
            const SizedBox(width: 16),
            Expanded(child: _buildLitersTextField(context)),
          ],
        ),
      );
    });
  }

  Widget _buildFuelTypeField(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          "Выберите тип топлива:",
          style: AppTextStyles.regular16,
        ),
        const SizedBox(height: 6),
        Container(
          color: AppColors.grey,
          child: GestureDetector(
            onTap: () {
              _openFuelTypePicker(context);
            },
            child: Observer(
              builder: (context) => GeneralTextField(
                contentPadding: const EdgeInsets.only(left: 8, top: 12),
                formName: _fuelTypeFormName,
                formKey: _formKey,
                focusNode: _fuelTypeFocusNode,
                enabled: false,
                hintText: _store(context).fuelType?.fuelName,
                hintColor: AppColors.textFieldColor,
                suffixIcon: const Icon(
                  Icons.expand_more_rounded,
                  size: 28,
                  color: AppColors.black,
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildDateField(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const SizedBox(height: 16),
          Text(
            "Введите дату:",
            style: AppTextStyles.regular16,
          ),
          const SizedBox(height: 6),
          Container(
            color: AppColors.grey,
            child: GeneralTextField(
              onChanged: (val) {
                _store(context).onChangeDate(valueOrEmpty(val));
              },
              contentPadding:
                  const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
              formName: _dateFormName,
              formKey: _formKey,
              focusNode: _dateFocusNode,
              controller: _dateController,
              hintText: "11.05.2023",
              hintColor: AppColors.textFieldColor,
              inputFormatters: [_dateFormatter],
              keyboardType: TextInputType.number,
              validator: FormBuilderValidators.compose([
                Validators.birthday,
              ]),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildSaveButton(BuildContext context) {
    return Positioned(
      bottom: 24,
      left: 24,
      right: 24,
      child: GeneralButtonWidget(
        borderRadius: 10,
        text: "Сохранить",
        minWidth: double.infinity,
        onTap: () {
          if (_formKey.currentState?.saveAndValidate() == true) {
            _store(context).saveTap();
            Navigator.of(context).pop();
          }
        },
      ),
    );
  }
}
