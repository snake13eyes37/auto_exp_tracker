import 'package:auto_exp_tracker/domain/expensive/expensive_type.dart';
import 'package:auto_exp_tracker/ui/widgets/bottom_sheet_header.dart';
import 'package:auto_exp_tracker/ui/widgets/general_radio_item.dart';
import 'package:flutter/material.dart';

class ExpensiveTypePickerPage extends StatelessWidget {
  final ExpensiveType? lastSelectedType;
  final Function(ExpensiveType) onGenderType;

  const ExpensiveTypePickerPage({
    Key? key,
    this.lastSelectedType,
    required this.onGenderType,
  }) : super(key: key);

  static MaterialPageRoute route({
    required Function(ExpensiveType) onGenderType,
    ExpensiveType? lastSelectedType,
  }) =>
      MaterialPageRoute(
        builder: (context) => ExpensiveTypePickerPage(
          lastSelectedType: lastSelectedType,
          onGenderType: onGenderType,
        ),
        fullscreenDialog: true,
      );

  @override
  Widget build(BuildContext context) {
    return _ExpensiveTypePickerScreen(
        lastSelectedType: lastSelectedType,
        onGenderType: onGenderType,
    );
  }
}

class _ExpensiveTypePickerScreen extends StatefulWidget {
  final ExpensiveType? lastSelectedType;
  final Function(ExpensiveType) onGenderType;

  const _ExpensiveTypePickerScreen({
    Key? key,
    this.lastSelectedType,
    required this.onGenderType,
  }) : super(key: key);

  @override
  _ExpensiveTypePickerScreenState createState() => _ExpensiveTypePickerScreenState();
}

class _ExpensiveTypePickerScreenState extends State<_ExpensiveTypePickerScreen> {
  ExpensiveType? selectedGender;

  void _goBack(context) {
    Navigator.pop(context);
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      selectedGender = widget.lastSelectedType;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          BottomSheetHeader(
            onCloseIconPressed: _goBack,
            title: "Выберите тип затраты",
          ),
          Expanded(
            child: ListView.separated(
              itemBuilder: _buildGenderOption,
              itemCount: ExpensiveType.values.length,
              separatorBuilder: (_, __) => Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: Divider(
                  height: .1,
                  color: Theme.of(context).dividerColor,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildGenderOption(BuildContext context, int index) {
    var gender = ExpensiveType.values[index];
    return GeneralRadioItem(
      onTap: () {
        setState(() {
          selectedGender = gender;
        });
        widget.onGenderType(gender);
        Navigator.of(context).pop();
      },
      isActive: gender.name == selectedGender?.name,
      title: gender.title,
    );
  }
}
