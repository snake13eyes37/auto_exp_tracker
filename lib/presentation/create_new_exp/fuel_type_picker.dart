import 'package:auto_exp_tracker/domain/expensive/expensive_type.dart';
import 'package:auto_exp_tracker/domain/model/fuel_type_enum.dart';
import 'package:auto_exp_tracker/ui/widgets/bottom_sheet_header.dart';
import 'package:auto_exp_tracker/ui/widgets/general_radio_item.dart';
import 'package:flutter/material.dart';

class FuelTypePickerPage extends StatelessWidget {
  final FuelType? lastSelectedType;
  final Function(FuelType) onGenderType;

  const FuelTypePickerPage({
    Key? key,
    this.lastSelectedType,
    required this.onGenderType,
  }) : super(key: key);

  static MaterialPageRoute route({
    required Function(FuelType) onGenderType,
    FuelType? lastSelectedType,
  }) =>
      MaterialPageRoute(
        builder: (context) => FuelTypePickerPage(
          lastSelectedType: lastSelectedType,
          onGenderType: onGenderType,
        ),
        fullscreenDialog: true,
      );

  @override
  Widget build(BuildContext context) {
    return _FuelTypePickerScreen(
      lastSelectedType: lastSelectedType,
      onGenderType: onGenderType,
    );
  }
}

class _FuelTypePickerScreen extends StatefulWidget {
  final FuelType? lastSelectedType;
  final Function(FuelType) onGenderType;

  const _FuelTypePickerScreen({
    Key? key,
    this.lastSelectedType,
    required this.onGenderType,
  }) : super(key: key);

  @override
  _FuelTypePickerScreenState createState() => _FuelTypePickerScreenState();
}

class _FuelTypePickerScreenState extends State<_FuelTypePickerScreen> {
  FuelType? selectedGender;

  void _goBack(context) {
    Navigator.pop(context);
  }

  @override
  void initState() {
    super.initState();
    setState(() {
      selectedGender = widget.lastSelectedType;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          BottomSheetHeader(
            onCloseIconPressed: _goBack,
            title: "Выберите тип топлива",
          ),
          Expanded(
            child: ListView.separated(
              itemBuilder: _buildGenderOption,
              itemCount: ExpensiveType.values.length,
              separatorBuilder: (_, __) => Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: Divider(
                  height: .1,
                  color: Theme.of(context).dividerColor,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildGenderOption(BuildContext context, int index) {
    var gender = FuelType.values[index];
    return GeneralRadioItem(
      onTap: () {
        setState(() {
          selectedGender = gender;
        });
        widget.onGenderType(gender);
        Navigator.of(context).pop();
      },
      isActive: gender.name == selectedGender?.name,
      title: gender.fuelName,
    );
  }
}
