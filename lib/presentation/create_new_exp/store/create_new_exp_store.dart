import 'dart:ffi';

import 'package:auto_exp_tracker/core/enviroment/environment.dart';
import 'package:auto_exp_tracker/core/extensions/extensions.dart';
import 'package:auto_exp_tracker/domain/expensive/expensive_type.dart';
import 'package:auto_exp_tracker/domain/model/expensive_model.dart';
import 'package:auto_exp_tracker/domain/model/fuel_type_enum.dart';
import 'package:auto_exp_tracker/domain/profile/models/profile_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dart_extensions/dart_extensions.dart';
import 'package:mobx/mobx.dart';

part 'create_new_exp_store.g.dart';

class CreateNewExpStore = CreateNewExpStoreBase with _$CreateNewExpStore;

abstract class CreateNewExpStoreBase with Store {
  @observable
  ExpensiveType expType = ExpensiveType.fuel;
  @observable
  String expName = "";
  @observable
  String range = "";
  @observable
  String date = "";

  @observable
  String price = "";
  @observable
  FuelType? fuelType;
  @observable
  int? liters;
  @observable
  String description = "";

  @action
  void onChangeName(String? value) {
    expName = valueOrEmpty(value);
  }

  @action
  void onChangeRange(String? value) {
    range = valueOrEmpty(value);
  }

  @action
  void onChangeDate(String value) {
    date = value;
  }

  @action
  void onChangePrice(String? value) {
    price = valueOrEmpty(value);
  }

  @action
  void onChangeFuelType(FuelType? value) {
    fuelType = value;
  }

  @action
  void onChangeLiters(String? value) {
    liters = int.tryParse(valueOrEmpty(value));
  }

  @action
  void onChangeDescription(String? value) {
    description = valueOrEmpty(value);
  }

  @action
  void onChangeExpType(ExpensiveType type) {
    expType = type;
  }

  @action
  void onSave() {}

  void initProfile() async {
    try {
      final firebaseInstant = FirebaseFirestore.instance;
      final userID = "NJkvSx3J6JNS628PjbtD"; //Environment.vin;
      final result = await firebaseInstant.collection("user").doc(userID).get();
      final data = result.data();
      if (data != null) {
        // profileModel = ProfileModel.fromMap(data);
        // final tempProfile = profileModel;
        // if (tempProfile != null) {
        //   name = tempProfile.name;
        //   lastName = tempProfile.lastName;
        //   userYears = tempProfile.years.toString();
        //   nameAuto = tempProfile.car.carType;
        //   range = tempProfile.car.range.toString();
        //   carYears = tempProfile.car.years.toString();toString
        // }
      }
    } catch (e) {
      //todo error
    }
  }

  @action
  void saveTap() {
    print("was tapped");
    save();
  }

  void save() async {
    final firebaseInstant = FirebaseFirestore.instance;
    final id = Environment.vin;
    print("daate: $date");
    final ExpensiveModel expensiveModel = ExpensiveModel(
      id: id,
      type: expType,
      description: description,
      expName: expType == ExpensiveType.fuel ? "Топливо" : expName,
      kilometers: valueOrZero(int.tryParse(range)),
      price: valueOrZero(int.tryParse(price)),
      date: DateTime.parse(date.split('.').reversed.join()),
      liters: expType == ExpensiveType.fuel ? liters : null,
      fuelType: expType == ExpensiveType.fuel ? fuelType : null,
    );
    final result = await firebaseInstant
        .collection("user")
        .doc(id)
        .collection(expType.toMap)
        .add(expensiveModel.withoutNullValue);
  }
}
