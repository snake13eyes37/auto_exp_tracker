// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_new_exp_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic, no_leading_underscores_for_local_identifiers

mixin _$CreateNewExpStore on CreateNewExpStoreBase, Store {
  late final _$expTypeAtom =
      Atom(name: 'CreateNewExpStoreBase.expType', context: context);

  @override
  ExpensiveType get expType {
    _$expTypeAtom.reportRead();
    return super.expType;
  }

  @override
  set expType(ExpensiveType value) {
    _$expTypeAtom.reportWrite(value, super.expType, () {
      super.expType = value;
    });
  }

  late final _$expNameAtom =
      Atom(name: 'CreateNewExpStoreBase.expName', context: context);

  @override
  String get expName {
    _$expNameAtom.reportRead();
    return super.expName;
  }

  @override
  set expName(String value) {
    _$expNameAtom.reportWrite(value, super.expName, () {
      super.expName = value;
    });
  }

  late final _$rangeAtom =
      Atom(name: 'CreateNewExpStoreBase.range', context: context);

  @override
  String get range {
    _$rangeAtom.reportRead();
    return super.range;
  }

  @override
  set range(String value) {
    _$rangeAtom.reportWrite(value, super.range, () {
      super.range = value;
    });
  }

  late final _$dateAtom =
      Atom(name: 'CreateNewExpStoreBase.date', context: context);

  @override
  String get date {
    _$dateAtom.reportRead();
    return super.date;
  }

  @override
  set date(String value) {
    _$dateAtom.reportWrite(value, super.date, () {
      super.date = value;
    });
  }

  late final _$priceAtom =
      Atom(name: 'CreateNewExpStoreBase.price', context: context);

  @override
  String get price {
    _$priceAtom.reportRead();
    return super.price;
  }

  @override
  set price(String value) {
    _$priceAtom.reportWrite(value, super.price, () {
      super.price = value;
    });
  }

  late final _$fuelTypeAtom =
      Atom(name: 'CreateNewExpStoreBase.fuelType', context: context);

  @override
  FuelType? get fuelType {
    _$fuelTypeAtom.reportRead();
    return super.fuelType;
  }

  @override
  set fuelType(FuelType? value) {
    _$fuelTypeAtom.reportWrite(value, super.fuelType, () {
      super.fuelType = value;
    });
  }

  late final _$litersAtom =
      Atom(name: 'CreateNewExpStoreBase.liters', context: context);

  @override
  int? get liters {
    _$litersAtom.reportRead();
    return super.liters;
  }

  @override
  set liters(int? value) {
    _$litersAtom.reportWrite(value, super.liters, () {
      super.liters = value;
    });
  }

  late final _$descriptionAtom =
      Atom(name: 'CreateNewExpStoreBase.description', context: context);

  @override
  String get description {
    _$descriptionAtom.reportRead();
    return super.description;
  }

  @override
  set description(String value) {
    _$descriptionAtom.reportWrite(value, super.description, () {
      super.description = value;
    });
  }

  late final _$CreateNewExpStoreBaseActionController =
      ActionController(name: 'CreateNewExpStoreBase', context: context);

  @override
  void onChangeName(String? value) {
    final _$actionInfo = _$CreateNewExpStoreBaseActionController.startAction(
        name: 'CreateNewExpStoreBase.onChangeName');
    try {
      return super.onChangeName(value);
    } finally {
      _$CreateNewExpStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void onChangeRange(String? value) {
    final _$actionInfo = _$CreateNewExpStoreBaseActionController.startAction(
        name: 'CreateNewExpStoreBase.onChangeRange');
    try {
      return super.onChangeRange(value);
    } finally {
      _$CreateNewExpStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void onChangeDate(String value) {
    final _$actionInfo = _$CreateNewExpStoreBaseActionController.startAction(
        name: 'CreateNewExpStoreBase.onChangeDate');
    try {
      return super.onChangeDate(value);
    } finally {
      _$CreateNewExpStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void onChangePrice(String? value) {
    final _$actionInfo = _$CreateNewExpStoreBaseActionController.startAction(
        name: 'CreateNewExpStoreBase.onChangePrice');
    try {
      return super.onChangePrice(value);
    } finally {
      _$CreateNewExpStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void onChangeFuelType(FuelType? value) {
    final _$actionInfo = _$CreateNewExpStoreBaseActionController.startAction(
        name: 'CreateNewExpStoreBase.onChangeFuelType');
    try {
      return super.onChangeFuelType(value);
    } finally {
      _$CreateNewExpStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void onChangeLiters(String? value) {
    final _$actionInfo = _$CreateNewExpStoreBaseActionController.startAction(
        name: 'CreateNewExpStoreBase.onChangeLiters');
    try {
      return super.onChangeLiters(value);
    } finally {
      _$CreateNewExpStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void onChangeDescription(String? value) {
    final _$actionInfo = _$CreateNewExpStoreBaseActionController.startAction(
        name: 'CreateNewExpStoreBase.onChangeDescription');
    try {
      return super.onChangeDescription(value);
    } finally {
      _$CreateNewExpStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void onChangeExpType(ExpensiveType type) {
    final _$actionInfo = _$CreateNewExpStoreBaseActionController.startAction(
        name: 'CreateNewExpStoreBase.onChangeExpType');
    try {
      return super.onChangeExpType(type);
    } finally {
      _$CreateNewExpStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void onSave() {
    final _$actionInfo = _$CreateNewExpStoreBaseActionController.startAction(
        name: 'CreateNewExpStoreBase.onSave');
    try {
      return super.onSave();
    } finally {
      _$CreateNewExpStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void saveTap() {
    final _$actionInfo = _$CreateNewExpStoreBaseActionController.startAction(
        name: 'CreateNewExpStoreBase.saveTap');
    try {
      return super.saveTap();
    } finally {
      _$CreateNewExpStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
expType: ${expType},
expName: ${expName},
range: ${range},
date: ${date},
price: ${price},
fuelType: ${fuelType},
liters: ${liters},
description: ${description}
    ''';
  }
}
