extension ListExt<T> on List<T> {
  List<T>? get nullIfEmpty {
    return isEmpty ? null : this;
  }
}

extension ListNullableExt<T> on List<T>? {
  List<T> addAndReturn(T value) {
    var list = this;
    if (list == null) {
      return List.of([value]);
    }
    list.add(value);
    return list;
  }
}
