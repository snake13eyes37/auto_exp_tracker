import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';

String valueOrEmpty(String? value, {String defaultValue = ''}) =>
    value ?? defaultValue;

T valueOrDefault<T>(T? value, T defaultValue) => value ?? defaultValue;

String? notEmptyValueOrNull(String value) => value.isEmpty ? null : value;

String? nullValueIfEmpty(String? value) =>
    value == null || value.isEmpty ? null : value;

int valueOrZero(int? value) => value ?? 0;

dynamic valueOrInt(double? value) {
  var result = value;
  if (result == null) {
    return null;
  }
  if (result.ceil() == result.floor()) {
    return result.toInt();
  }
  return result;
}

double valueOrDoubleZero(double? value) => value ?? 0.0;

int valueDoubleToIntOrZero(double? value) => value?.toInt() ?? 0;

bool valueOrFalse(bool? flag) => flag ?? false;

String concatWithSymbol(List<String?> args, {String symbol = ','}) {
  return args.whereType<String>().join(symbol);
}

List<T> valueOrEmptyList<T>(List<T>? list) => list ?? [];

extension StringExt on String {
  String get capitalize {
    if (isEmpty) {
      return this;
    }
    var string = toLowerCase();
    return string[0].toUpperCase() + string.substring(1);
  }

  String removeYearSuffix() {
    return replaceAll(' г.', '');
  }

  DateTime parseToDDMMYYY() {
    return DateFormat('yyyy-MM-dd').parse(this);
  }

  bool get isNumeric {
    return int.tryParse(this) != null;
  }

  String get mediaImage => replaceFirst("media/", "media/medium-quality/");
}

extension GlobalKeyExtension on GlobalKey {
  Rect? get globalPaintBounds {
    final renderObject = currentContext?.findRenderObject();
    final translation = renderObject?.getTransformTo(null).getTranslation();
    if (translation != null && renderObject?.paintBounds != null) {
      final offset = Offset(translation.x, translation.y);
      return renderObject!.paintBounds.shift(offset);
    } else {
      return null;
    }
  }
}
