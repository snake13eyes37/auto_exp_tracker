import 'package:auto_exp_tracker/domain/expensive/expensive_type.dart';
import 'package:auto_exp_tracker/domain/model/expensive_model.dart';
import 'package:auto_exp_tracker/domain/profile/models/car_model.dart';
import 'package:auto_exp_tracker/domain/profile/models/profile_model.dart';

class AppMocks {
  AppMocks._();

  static CarModel car = const CarModel(
    vin: "XDFGF45DX56GFD",
    carType: "Lada Vesta",
    years: 2017,
    range: 125456,
    url:
        "https://www.google.com/url?sa=i&url=https%3A%2F%2Fwww.lamborghini.com%2Fru-en%2F%25D0%25BC%25D0%25BE%25D0%25B4%25D0%25B5%25D0%25BB%25D0%25B8%2Furus&psig=AOvVaw0y05l6e-p9rOPx9sp39ms2&ust=1682094849806000&source=images&cd=vfe&ved=0CBEQjRxqFwoTCOjn0eHxuP4CFQAAAAAdAAAAABAN",
    yearsBuy: 2,
  );

  static ProfileModel profileModel = ProfileModel(
    name: "Kirilll",
    lastName: "Lykov",
    years: 21,
    car: car,
  );

  static List<ExpensiveModel> expensiveList = [
    // const ExpensiveModel(
    //   type: ExpensiveType.fuel,
    //   description: "fuel",
    //   range: 2000,
    //   price: 1500,
    // ),
    // const ExpensiveModel(
    //   type: ExpensiveType.consumables,
    //   description: "consumables",
    //   range: 2000,
    //   price: 15000,
    // ),
    // const ExpensiveModel(
    //   type: ExpensiveType.service,
    //   description: "technical",
    //   range: 2000,
    //   price: 3000,
    // ),
    // const ExpensiveModel(
    //   type: ExpensiveType.changeDetails,
    //   description: "details",
    //   range: 2000,
    //   price: 20000,
    // ),
    // const ExpensiveModel(
    //   type: ExpensiveType.other,
    //   description: "other",
    //   range: 2000,
    //   price: 1000,
    // ),
  ];
}
